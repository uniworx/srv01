{ config, lib, pkgs, ... }:

with lib;

let
  reUUID = "[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}";
in {
  config = {
    services.k3s = {
      enable = true;
      role = "server";
      extraFlags = toString [
        "--container-runtime-endpoint unix:///run/containerd/containerd.sock"
        "--kubelet-arg=\"runtime-request-timeout=1h\"" # https://serverfault.com/q/1062564
        "--disable-cloud-controller"
        "--disable local-storage"
        "--disable traefik"
        "--disable servicelb"
        # "--kubelet-arg=\"node-ip=::\""
        # "--cluster-cidr=10.42.0.0/16,2001:cafe:42:0::/56" "--service-cidr=10.43.0.0/16,2001:cafe:42:1::/112"
      ];
    };

    environment.systemPackages = with pkgs; [ k3s ];

    virtualisation.containerd = {
      enable = true;
      settings =
        let
          fullCNIPlugins = pkgs.buildEnv {
            name = "full-cni";
            paths = with pkgs; [
              cni-plugins
              cni-plugin-flannel
            ];
          };
        in {
          plugins."io.containerd.grpc.v1.cri".cni = {
            bin_dir = "${fullCNIPlugins}/bin";
            conf_dir = "/var/lib/rancher/k3s/agent/etc/cni/net.d/";
          };
        };
    };

    systemd.services.k3s = {
      path = with pkgs; [ ipset ];
      preStart =
        let
          dockerImages = [
            (pkgs.dockerTools.pullImage {
              imageName = "ubuntu";
              imageDigest = "sha256:8aa9c2798215f99544d1ce7439ea9c3a6dfd82de607da1cec3a8a2fae005931b";
              sha256 = "0xjxih1rdwbdw937s41jmy9d93n1dv8mh5fqvcggcn7aw05yjvl7";
              finalImageName = "ubuntu";
              finalImageTag = "18.04";
            })
            (pkgs.dockerTools.pullImage {
              imageName = "fpco/stack-build";
              imageDigest = "sha256:7744a328643d261972f15786db53f34374cf27a440b4843ddd861a680ff7f79a";
              sha256 = "1db2pjkfhcbb8xlnj2x2ld767cad79n0vzaafvx4m5xib0abc25v";
              finalImageName = "fpco/stack-build";
              finalImageTag = "lts-18.0";
            })
          ];

          manifests = [
            { path = ./metallb.yml;
            }
            { path = ./zfs-provisioner.yml;
              substituteArgs = [
                "--subst-var-by" "zfs-provision-key" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/          /;' ${config.sops.secrets."zfs-provision-key".path})\""
                "--subst-var-by" "cert-authority-pubkey" "\"$(cat ${../ssh/ca.pub})\""
              ];
            }
            { path = ./gitlab.yml;
              substituteArgs = [
                "--subst-var-by" "gitlab-postgresql-password" "\"$(cat ${config.sops.secrets."gitlab-postgresql-password".path})\""
                "--subst-var-by" "gitlab-chart-content" "\"$(base64 -w0 ${patchedGitlabChart})\""
                "--subst-var-by" "mail-ca" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/          /;' ${../email/ca/ca.crt})\""
                "--subst-var-by" "mail-crt" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/          /;' ${../email/ca/gitlab.crt})\""
                "--subst-var-by" "mail-key" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/          /;' ${config.sops.secrets."gitlab-email-key".path})\""
                "--subst-var-by" "rsa-pub" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/    /;' ${../ssh/gitlab-host_rsa_key.pub})\""
                "--subst-var-by" "rsa-cert" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/    /;' ${../ssh/gitlab-host_rsa_key-cert.pub})\""
                "--subst-var-by" "rsa-priv" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/    /;' ${config.sops.secrets."gitlab-rsa-hostkey".path})\""
                "--subst-var-by" "ed25519-pub" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/    /;' ${../ssh/gitlab-host_ed25519_key.pub})\""
                "--subst-var-by" "ed25519-cert" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/    /;' ${../ssh/gitlab-host_ed25519_key-cert.pub})\""
                "--subst-var-by" "ed25519-priv" "\"$(${pkgs.gnused}/bin/sed '2,$s/^/    /;' ${config.sops.secrets."gitlab-ed25519-hostkey".path})\""
              ];
            }
          ];

          patchedGitlabChart = pkgs.stdenv.mkDerivation rec {
            name = "${pname}-${version}.tgz";
            pname = "gitlab";
            version = "6.11.2";

            src = pkgs.fetchurl {
              url = "https://gitlab-charts.s3.amazonaws.com/${pname}-${version}.tgz";
              hash = "sha256-+74kgp9byLNqT27LVVGPxZjTL3sApnsRDRb7fOZg1sg=";
            };
            patches = [
              ./gitlab-chart.patch
              ./gitlab-chart_sshd.patch
            ];

            nativeBuildInputs = with pkgs; [ coreutils-full ];

            buildPhase = "true";
            installPhase = ''
              tar -cvzf $out --transform 's/^\./gitlab/' .
            '';
          };
        in ''
          source ${../substitute.sh}

          rm -rf /var/lib/rancher/k3s/agent/images/nixos
          mkdir -p /var/lib/rancher/k3s/agent/images/nixos
          ${concatMapStringsSep "\n" (imagePath: ''
            ln -sfv ${imagePath} /var/lib/rancher/k3s/agent/images/nixos
          '') dockerImages}

          rm -rf /var/lib/rancher/k3s/server/manifests/nixos
          mkdir -p /var/lib/rancher/k3s/server/manifests/nixos
          ${concatMapStringsSep "\n" ( { path, substituteArgs ? [] }: ''
            substitute ${path} /var/lib/rancher/k3s/server/manifests/nixos/$(basename ${path}) \
              ${concatStringsSep " \\\n  " substituteArgs}
          '') manifests}
        '';
    };

    sops.secrets = {
      "gitlab-postgresql-password" = {
        sopsFile = ./gitlab-postgresql-password;
        format = "binary";
      };
      "zfs-provision-key" = {
        sopsFile = ./zfs-provision-key;
        format = "binary";
      };
      "gitlab-email-key" = {
        sopsFile = ../email/ca/gitlab.key;
        format = "binary";
      };
      "gitlab-rsa-hostkey" = {
        sopsFile = ../ssh/gitlab-host_rsa_key;
        format = "binary";
      };
      "gitlab-ed25519-hostkey" = {
        sopsFile = ../ssh/gitlab-host_ed25519_key;
        format = "binary";
      };
    };

    users.users."zfs-provisioner" = {
      group = "zfs-provisioner";
      isSystemUser = true;
      openssh.authorizedKeys.keys =
        let
          forcedScript = pkgs.writeShellScript "forced-command" ''
            whitelist=(
              "^sudo -H zfs create (-o [^ ]+ )*srv01/(local|safe)/var-lib-kubernetes/pvc-${reUUID}$"
              "^sudo -H zfs get -Hp all srv01/(local|safe)/var-lib-kubernetes/pvc-${reUUID}$"
              "^sudo -H zfs destroy -r srv01/local/var-lib-kubernetes/pvc-${reUUID}$"
              "^sudo -H chmod g\+w /var/lib/kubernetes/(local|safe)/pvc-${reUUID}$"
            )

            match='''
            for re in $whitelist; do
              if [[ "''${SSH_ORIGINAL_COMMAND}" =~ $re ]]; then
                match=yes
                break
              fi
            done
            [[ -n "$match" ]] || exit 2

            export PATH=${config.security.wrapperDir}:${config.boot.zfs.package}/bin:${pkgs.coreutils}/bin

            exec -- ''${SSH_ORIGINAL_COMMAND}
          '';
        in [
          "restrict,command=\"${forcedScript}\" ${builtins.readFile ./zfs-provision-key.pub}"
        ];
      shell = pkgs.bashInteractive;
    };
    users.groups."zfs-provisioner" = {};
    security.sudo.extraRules = [
      { users = [ "zfs-provisioner" ];
        commands = [
          { command = "${config.boot.zfs.package}/bin/zfs ^create (-o [^ ]+ )*srv01/(local|safe)/var-lib-kubernetes/pvc-${reUUID}$"; options = [ "NOPASSWD" ]; }
          { command = "${config.boot.zfs.package}/bin/zfs ^get -Hp all srv01/(local|safe)/var-lib-kubernetes/pvc-${reUUID}$"; options = [ "NOPASSWD" ]; }
          { command = "${config.boot.zfs.package}/bin/zfs ^destroy -r srv01/local/var-lib-kubernetes/pvc-${reUUID}$"; options = [ "NOPASSWD" ]; }
          { command = "${pkgs.coreutils}/bin/chmod ^g\\+w /var/lib/kubernetes/(local|safe)/pvc-${reUUID}$"; options = [ "NOPASSWD" ]; }
        ];
      }
    ];
  };
}
