# NixOS Configuration for `srv01.uniworx.de`

## Configured services/roles

  - provide authoritative DNS for `uniworx.de`
  - provide email services for `uniworx.de` (IMAP (incl. SIEVE), SMTP (incoming, outgoing, & submission))

In the future we also expect this server to:

  - host an (at first) minimal website so `https://uniworx.de` works at all
  - host a private GitLab instance
  - provide CI/CD to said custom GitLab instance
  - provide developer environments for work on projects hosted in said custom GitLab instance (GitPod)

## Usage of this flake

All commands use `.` as the location of this flake.  
This works if they are executed within a clone of this git repository.

Enter a development environment that provides tooling that is useful to work on the contents of this repository (i.e. `sops`) with either `direnv allow` (if installed locally) or `nix develop`

Build the configuration (e.g. to see if it evaluates at all; `nom` provides prettier output than `nix` and is provided in the development environment):
```bash
nom build .#nixosConfigurations.srv01.config.system.build.toplevel
```

To use this flake for `nixos-rebuild` pass it as an argument to `--flake`; `--refresh` causes nix to redownload the flake to check if it has changed:
```bash
nixos-rebuild --refresh --flake 'git+https://gitlab.com/uniworx/srv01' switch
```

NixOS hosts built using this flake have a reference to the above url in their [registry](https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-registry.html).  
Therefore the following should also work:
```bash
nixos-rebuild --refresh --flake machines switch
```

To use a specific branch of this repository:
```bash
nixos-rebuild --refresh --flake 'git+https://gitlab.com/uniworx/srv01?ref=branch-name' switch
```
