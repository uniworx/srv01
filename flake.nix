{
  # Inputs to the flake are automagically locked to a specific
  # revision when flake outputs are built (see flake.lock in this
  # directory)
  # Inputs can be updated easily with `nix flake update`.
  # This is the primary benefit of having a flake in the first place.
  # No more dealing with channels.
  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "22.11";
    };

    home-manager = {
      type = "github";
      owner = "nix-community";
      repo = "home-manager";
      ref = "release-22.11";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    sops-nix = {
      type = "github";
      owner = "Mic92";
      repo = "sops-nix";
      ref = "master";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

    # Utilities hosted elsewhere, see `shell.nix`
    ca-util = {
      type = "gitlab";
      owner = "gkleen";
      repo = "ca";
      ref = "v2.0.4";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    backup-utils = {
      type = "gitlab";
      owner = "gkleen";
      repo = "backup-utils";
      ref = "v0.1.1";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    k8s-gitlab-borg = {
      type = "gitlab";
      host = "gitlab.uniworx.de";
      owner = "uniworx";
      repo = "k8s-gitlab-borg";
      ref = "v2.0.0";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

    # For building python utilities, e.g. email/ccert-policy-server
    pypi-deps-db = {
      type = "github";
      owner = "DavHau";
      repo = "pypi-deps-db";
      ref = "e9571cac25d2f509e44fec9dc94a3703a40126ff";
      flake = false;
    };
    mach-nix = {
      type = "github";
      owner = "DavHau";
      repo = "mach-nix";
      ref = "65266b5cc867fec2cb6a25409dd7cd12251f6107";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        pypi-deps-db.follows = "pypi-deps-db";
      };
    };
  };

  outputs = { self, nixpkgs, ... }@inputs: {
    # Provide an overridable (arguments provided as attrset below may
    # be replaced in later uses) version of the nixpkgs input to use
    # as pkgs in system configurations and e.g. the development shell
    legacyPackages."x86_64-linux" = (nixpkgs.lib.makeOverridable (import (nixpkgs.outPath + "/pkgs/top-level"))) {
      localSystem = "x86_64-linux";
    };

    # System configurations provided by this flake
    nixosConfigurations = {
      srv01 = nixpkgs.lib.nixosSystem rec {
        # Additional arguments passed to all modules of this
        # configuration
        # Analogous to pkgs, lib, etc.
        specialArgs = {
          flakeInputs = inputs;
          flake = self;
        };

        # Import actual configuration from separate file for
        # readability
        modules = [ ./configuration.nix ];
      };
    };

    # Development shell for use with `direnv` and `nix develop`
    devShells."x86_64-linux".default = import ./shell.nix inputs;
  };
}
