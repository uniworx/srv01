{ config, pkgs, lib, ... }:

with lib;

let
  psql-versioning = pkgs.fetchgit {
    url = "https://gitlab.com/depesz/Versioning";
    rev = "5f458f58aa782ee09febc9643fa8741cb8750727";
    sha256 = "sha256-QcOm85h0aSHOAMmOCOddwJAg3mYkGhrJMzKrs3VNjrg=";
  };
in {
  imports = [
    ./pgbackrest_module.nix
  ];

  config = {
    # PostgreSQL
    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_15;
      settings = {
        listen_addresses = mkForce (concatStringsSep "," [
          "localhost"
          "10.42.0.1"
        ]);
      };
      authentication = ''
        host sameuser gitlab 10.42.0.0/16 scram-sha-256
      '';
    };

    # Backups for postgresql via pgBackRest
    services.pgbackrest = {
      enable = true;
      settings = {
        "srv01.uniworx.de" = {
          pg1-path = config.services.postgresql.dataDir;

          # Create backups locally
          repo1-path = "/var/lib/pgbackrest";
          repo1-retention-full-type = "time";
          repo1-retention-full = 7;
          repo1-retention-archive = 2;

          # And also send backups to vidhar
          repo2-host-type = "tls";
          repo2-host = "vidhar.yggdrasil.li";
          repo2-host-ca-file = toString ./pgbackrest-ca.crt;
          repo2-host-cert-file = toString ./pgbackrest.crt;
          repo2-host-key-file = config.sops.secrets."pgbackrest.key".path;
          repo2-retention-full-type = "time";
          repo2-retention-full = 14;
          repo2-retention-archive = 7;
        };

        "global" = {
          # Aggressively compress backups
          compress-type = "zst";
          compress-level = 9;

          # Don't slow down postgresql with compression
          archive-async = true;
          spool-path = "/var/spool/pgbackrest";
        };

        # Listen on port 8432 for connections by vidhar
        # Vidhar pulls a full backup over one of these connections daily
        "global:server" = {
          tls-server-address = "2a03:4000:5e:e55::";
          tls-server-ca-file = toString ./pgbackrest-ca.crt;
          tls-server-cert-file = toString ./pgbackrest.crt;
          tls-server-key-file = config.sops.secrets."pgbackrest.key".path;
          tls-server-auth = ["vidhar.yggdrasil=srv01.uniworx.de"];
        };

        "global:archive-push" = {
          process-max = 7;
        };
        "global:archive-get" = {
          process-max = 7;
        };
      };

      # Listen on port 8432 for connections by vidhar
      tlsServer.enable = true;

      # Create a daily full backup locally
      backups."srv01.uniworx.de-daily" = {
        stanza = "srv01.uniworx.de";
        repo = "1";
        timerConfig.OnCalendar = "daily";
      };

      # Instruct postgresql to send WALs to pgBackRest
      configurePostgresql = {
        enable = true;
        stanza = "srv01.uniworx.de";
      };
    };

    sops.secrets."pgbackrest.key" = {
      format = "binary";
      sopsFile = ./pgbackrest.key;
      owner = "postgres";
      group = "postgres";
      mode = "0400";
    };

    # Perform schema migrations whenever postgresql starts
    #
    # Execute all files in `migrations` in order and make an upstream
    # versioning script available to them.
    #
    # Migrations are commonly transactions that check whether they
    # have already been applied using the supplied script and abort
    # the transaction if they have.
    # They are thus made idempotent and may additionally specify
    # inter-migration dependencies.
    systemd.services.migrate-postgresql = {
      after = [ "postgresql.service" ];
      bindsTo = [ "postgresql.service" ];
      wantedBy = [ "postgresql.service" ];

      serviceConfig = {
        Type = "oneshot";
        inherit (config.systemd.services.postgresql.serviceConfig) User Group;
        RemainAfterExit = true;

        LoadCredential = [
          "gitlab-postgresql-password:${config.sops.secrets."gitlab-postgresql-password".path}"
        ];
        RuntimeDirectory = [ "migrate-postgresql" ];
      };

      path = [ config.services.postgresql.package ];
      script =
        let
          migrationFiles =
            let
              dir = ./migrations;
              toMigrationFile = name: v:
                if v == "regular" || v == "symlink"
                then {
                  path = dir + "/${name}";
                  inherit name;
                  database =
                    let
                      matchRes = builtins.match "[0-9]+-([^.]+)(\.sql)?" name;
                    in assert assertMsg (matchRes != null) "Filename of PostgreSQL migration ‘${name}’ does not match [0-9]+-(.+)"; elemAt matchRes 0;
                }
                else null;
            in filter (v: v != null) (mapAttrsToList toMigrationFile (builtins.readDir dir));
          sortedMigrations = sort (a: b: lessThan a.name b.name) migrationFiles;
          toMigrationScript = name: path: pkgs.runCommand name {} ''
            cat > $out <<EOF
            \i ${psql-versioning + "/install.versioning.sql"}
            EOF
            cat ${path} >> $out
          '';
          substituteArgs = [
            "--subst-var-by" "gitlab-postgresql-password" "$(cat $CREDENTIALS_DIRECTORY/gitlab-postgresql-password)"
          ];
        in ''
          source ${../substitute.sh}

          ${concatMapStringsSep "\n" ({ path, database, name, ... }: ''
            script=${toMigrationScript name path}
            substitute ''${script} $RUNTIME_DIRECTORY/$(basename ''${script}) \
              ${concatStringsSep " \\\n  " substituteArgs}
            psql ${database} postgres -eXf $RUNTIME_DIRECTORY/$(basename ''${script})
          '') sortedMigrations}
        '';
    };
  };
}
