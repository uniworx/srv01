# PostgreSQL

This server runs a PostgreSQL database-server and provides it's
service to the following services:

  - Email services (Dovecot2, Postfix, ...)
  - NextCloud

## Migration

We use [an upstream versioning script][versioning] to handle
migrations in the database schema including initial creation.

Migrations are stored in `migrations` and executed in order by the
systemd unit `migrate-postgresql.service`, which is triggered whenever
postgresql starts.

[versioning]: https://gitlab.com/depesz/Versioning

## Backup

We use [pgBackRest][] for backup purposes.

[pgBackRest]: https://github.com/pgbackrest/pgbackrest

pgBackRest has the concept of repositories, which contain backup data
for a given database.  
There is a local repository on this server and a remote repository
currently on viðar (infrastructure that belongs to @gkleen).

When PostgreSQL switches to a new WAL segment (which it does after a
timeout even if there is no activity) the old one is pushed to both
repositories.  
This provides [PITR][] capability.

[PITR]: https://www.postgresql.org/docs/current/continuous-archiving.html

Additionally this server and viðar both create daily backups of the
full contents of the database.  
These can be used to greatly speed up PITR.
