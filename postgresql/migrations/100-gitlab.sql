BEGIN;
SELECT _v.register_patch('000-gitlab', null, null);

CREATE USER "gitlab" WITH PASSWORD '@gitlab-postgresql-password@';
GRANT CONNECT ON DATABASE "gitlab" TO "gitlab";
GRANT ALL ON SCHEMA public TO "gitlab";
COMMIT;

BEGIN;
SELECT _v.register_patch('001-create', ARRAY['000-gitlab'], null);

GRANT CREATE ON DATABASE "gitlab" TO "gitlab";
COMMIT;
