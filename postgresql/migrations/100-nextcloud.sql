BEGIN;
SELECT _v.register_patch('000-nextcloud', null, null);

CREATE USER "nextcloud";
GRANT CONNECT ON DATABASE "nextcloud" TO "nextcloud";
GRANT ALL ON SCHEMA public TO "nextcloud";
COMMIT;
