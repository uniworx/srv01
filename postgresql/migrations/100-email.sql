BEGIN;
SELECT _v.register_patch('000-users', null, null);

CREATE USER "postfix";
GRANT CONNECT ON DATABASE "email" TO "postfix";
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO "postfix";
CREATE USER "dovecot2";
GRANT CONNECT ON DATABASE "email" TO "dovecot2";
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO "dovecot2";
CREATE USER "postfix-ccert-sender-policy";
GRANT CONNECT ON DATABASE "email" TO "postfix-ccert-sender-policy";
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO "postfix-ccert-sender-policy";
COMMIT;


BEGIN;
SELECT _v.register_patch('001-citext', null, null);

CREATE EXTENSION citext;
COMMIT;

BEGIN;
SELECT _v.register_patch('002-base', null, null);

CREATE TABLE mailbox (
  id uuid DEFAULT gen_random_uuid() NOT NULL,
  mailbox citext NOT NULL,
  quota_bytes bigint,
  CONSTRAINT mailbox_non_empty CHECK (mailbox <> ''),
  CONSTRAINT quota_bytes_positive CHECK (
    CASE
      WHEN (quota_bytes IS NOT NULL) THEN quota_bytes > 0
      ELSE true
    END)
);

CREATE TABLE mailbox_mapping (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    local citext,
    domain citext NOT NULL,
    mailbox uuid,
    extension citext,
    reject boolean DEFAULT false NOT NULL,
    CONSTRAINT domain_non_empty CHECK (domain <> ''),
    CONSTRAINT local_non_empty CHECK (local IS DISTINCT FROM ''),
    CONSTRAINT mailbox_mapping_extension_check CHECK (
      CASE
        WHEN (extension IS NOT NULL) THEN (extension !~~ '%+%') AND (extension <> '') AND (local IS DISTINCT FROM '')
	ELSE true
      END)
);

CREATE VIEW mailbox_quota_rule AS
  SELECT
    id,
    mailbox,
    CASE
      WHEN (mailbox.quota_bytes IS NULL) THEN '*:ignore'
      ELSE '*:bytes=' || mailbox.quota_bytes
    END AS quota_rule
  FROM mailbox;

CREATE VIEW imap_user AS
  SELECT
    mailbox AS "user",
    quota_rule
  FROM mailbox_quota_rule;

CREATE VIEW lmtp_mapping AS
  SELECT
    mailbox_quota_rule.mailbox AS "user",
    mailbox_quota_rule.quota_rule,
    mailbox_mapping.local,
    mailbox_mapping.extension,
    mailbox_mapping.domain
  FROM (
         mailbox_quota_rule
    JOIN mailbox_mapping
      ON mailbox_quota_rule.id = mailbox_mapping.mailbox
  );

CREATE VIEW virtual_mailbox_access AS
  SELECT
       (CASE
          WHEN (local IS NULL) THEN ''
          ELSE local
        END)
    || (CASE
          WHEN (extension IS NULL) THEN ''
          ELSE '+' || extension
        END)
    || '@' || domain AS lookup,
    CASE
      WHEN (mailbox IS NULL OR reject) THEN 'REJECT'
      ELSE 'DUNNO'
    END AS action
  FROM mailbox_mapping;

CREATE VIEW virtual_mailbox_domain AS
  SELECT
    DISTINCT domain
  FROM mailbox_mapping;

CREATE VIEW virtual_mailbox_mapping AS
  SELECT
       (CASE
         WHEN (local IS NULL) THEN ''
         ELSE local
       END)
    ||
       (CASE
          WHEN (extension IS NULL) THEN ''
          ELSE '+' || extension
       END)
    || '@' || domain AS lookup
  FROM mailbox_mapping
  WHERE mailbox_mapping.mailbox IS NOT NULL;
COMMIT;
