{ pkgs, lib, ... }:
{
  users.users.jost = {
    description = "Steffen Jost";
    group = "users";
    createHome = true;
    shell = "${pkgs.zsh}/bin/zsh";
    isNormalUser = true;
    openssh.authorizedKeys.keyFiles = let dir = ./authorized-keys; in lib.mapAttrsToList (n: _: dir + "/${n}") (builtins.readDir dir);
  };

  home-manager.users.jost = {
    programs = {
      git = {
        enable = true;
        userEmail = "S.Jost@Fraport.de";
        userName = "Steffen Jost";
        extraConfig = {
          pull.rebase = false;
          submodule.recurse = true;
        };
      };

      ssh = {
        enable = true;
        hashKnownHosts = true;
        matchBlocks = {
          "gitlab.ifi.lmu.de" = {
            user = "git";
          };
        };
        extraConfig = ''
          IdentitiesOnly true
        '';
      };

      zsh = {
        enable = true;
        initExtra = ''
          if [[ $1 == eval ]]
          then
            "$@"
          set --
          fi
          if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
            exec tmux
          fi
        '';
        initExtraFirst = lib.mkBefore ''
          if [[ $TERM == "dumb" ]]; then
            unsetopt zle
            PS1='$ '
            eval "$(${pkgs.direnv}/bin/direnv hook zsh)"
            return
          fi
        '';
        oh-my-zsh = {
          enable = true;
          theme = "agnoster";
          plugins = [ "git" ];
        };
      };

      tmux = {
        enable = true;
        clock24 = true;
        historyLimit = 50000;
        extraConfig = lib.readFile (pkgs.stdenv.mkDerivation {
          inherit (pkgs) zsh;
          name = "tmux.conf";
          src = ./dotfiles/tmux.conf;
          buildInputs = with pkgs; [ makeWrapper ];
          phases = [ "installPhase" ];
          mandb = pkgs.man-db;
          installPhase = ''
            substituteAll $src $out
          '';
        });
      };

      neovim = {
        enable = true;
        vimAlias = true;
        viAlias = true;
        # defaultEditor = true;
        plugins = with pkgs.vimPlugins; [
          airline
          fugitive
          vim-nix
        ];
        extraConfig = builtins.readFile ./dotfiles/vim/.vimrc;
      };

      direnv = {
        enable = true;
        nix-direnv.enable = true;
      };
    };

    home.sessionVariables = {
    # TERM = "screen-256color";
    # EDITOR = "${editor}/bin/editor";
    # GIT_EDITOR = "${editor}/bin/editor";
    };

    home.packages = with pkgs; [
    ];

    home.stateVersion = "22.11";
    systemd.user.startServices = "sd-switch";
  };
}
