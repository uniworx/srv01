{ pkgs, lib, ... }:

let
  editor = pkgs.runCommand "editor" {
    inherit (pkgs) emacs vim;
    buildInputs = with pkgs; [ makeWrapper ];
  } ''
    mkdir -p $out/bin
    makeWrapper \
      $emacs/bin/emacsclient \
      $out/bin/editor \
        --suffix PATH : ${lib.makeBinPath (with pkgs; [ emacs vim ])} \
        --add-flags "-s ~/.ssh/emacs-server -T /ssh:srv01.uniworx.de -a vim"
  '';
in {
  users.users.gkleen = {
    description = "Gregor Kleen";
    extraGroups = [ "wheel" ];
    shell = "${pkgs.zsh}/bin/zsh";
    isNormalUser = true;
    hashedPassword = "$6$rounds=2500000$a.sL740Ano/76EHU$2xyJIpvxHeWi40BZl66tPuAZGTa636TGxKbaBXFaAfnYWflKGWETn9qzmI73PwT5drvqFHK2wAXQnFEKAhk96/";
  };

  home-manager.users.gkleen = {
    imports = [
      ./zsh ./tmux
    ];

    config = {
      programs = {
        git = {
          enable = true;
          userEmail = "gregor@kleen.consulting";
          userName = "Gregor Kleen";
          delta.enable = true;
          extraConfig = {
            pull.rebase = false;
            submodule.recurse = true;
          };
        };

        ssh = {
          enable = true;
          hashKnownHosts = true;
          extraConfig = ''
            IdentitiesOnly true
          '';
          matchBlocks = {
            "gitlab.ifi.lmu.de" = {
              user = "git";
              identityFile = "~/.ssh/kleen@gitlab.ifi.lmu.de";
            };
          };
        };

        gpg.enable = true;

        direnv = {
          enable = true;
          nix-direnv.enable = true;
        };
      };

      home.packages = with pkgs; [
        mosh glab
      ];

      home.sessionVariables = {
        EDITOR = "${editor}/bin/editor";
      };

      home.stateVersion = "22.11";
      systemd.user.startServices = "sd-switch";
    };
  };
}
