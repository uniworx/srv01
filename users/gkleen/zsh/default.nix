{ config, pkgs, lib, ... }:
let
  cfg = config.programs.zsh;
  p10kZsh = "${cfg.dotDir}/.p10k.zsh";
in {
  config = {
    programs.zsh = {
      enable = true;
      dotDir = ".config/zsh";
      autocd = true;
      enableCompletion = true;

      plugins = [
        { name = "powerlevel10k";
          file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
          src = pkgs.zsh-powerlevel10k;
        }
      ];
      initExtraBeforeCompInit = ''
        source "${config.home.homeDirectory}/${p10kZsh}"
      '';
      initExtraFirst = lib.mkBefore ''
        if [[ $TERM == "dumb" ]]; then
          unsetopt zle
          PS1='$ '
          ${lib.optionalString config.programs.direnv.enable ''
            eval "$(${pkgs.direnv}/bin/direnv hook zsh)"
          ''}
          return
        fi
      '';
      initExtra = lib.mkAfter ''
        source ${./zshrc}
        source "${pkgs.zsh-syntax-highlighting}/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
      '';
    };

    home.file.${p10kZsh}.source = ./p10k.zsh;
  };
}
