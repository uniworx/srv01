{ config, pkgs, lib, ... }:
{
  config = {
    programs.tmux = {
      enable = true;
      clock24 = true;
      historyLimit = 50000;
      extraConfig = lib.readFile (pkgs.runCommand "tmux.conf" {
        inherit (pkgs) zsh;
      } "substituteAll ${./tmux.conf} $out");
    };
  };
}
