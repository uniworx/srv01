{ pkgs, lib, ... }:
{
  users.users.savau = {
    description = "Sarah Vaupel";
    extraGroups = [ "wheel" ];
    group = "users";
    createHome = true;
    shell = "${pkgs.zsh}/bin/zsh";
    isNormalUser = true;
    hashedPassword = "$6$rounds=500000$Z2bUsTQQSa2y$u1Sn2j/YgyvkNLopjKqxXe8I9fOozNIoN7GRE7aCR3AnJRh4m8sMAy3d4MjrsNkkaQiHBpf4ncp7n3ix4Hlfz.";
  };

  home-manager.users.savau = {
    programs = {
      git = {
        enable = true;
        userEmail = "sarah.vaupel@uniworx.de";
        userName = "Sarah Vaupel";
        extraConfig = {
          pull.rebase = false;
          submodule.recurse = true;
        };
      };

      ssh = {
        enable = true;
        hashKnownHosts = true;
        matchBlocks = {
          "gitlab.ifi.lmu.de" = {
            user = "git";
          };
        };
        extraConfig = ''
          IdentitiesOnly true
        '';
      };

      zsh = {
        enable = true;
        initExtra = ''
          if [[ $1 == eval ]]
          then
            "$@"
          set --
          fi
          if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
            exec tmux
          fi
        '';
        initExtraFirst = lib.mkBefore ''
          if [[ $TERM == "dumb" ]]; then
            unsetopt zle
            PS1='$ '
            eval "$(${pkgs.direnv}/bin/direnv hook zsh)"
            return
          fi
        '';
        oh-my-zsh = {
          enable = true;
          theme = "agnoster";
          plugins = [ "git" ];
        };
      };

      tmux = {
        enable = true;
        clock24 = true;
        historyLimit = 50000;
        extraConfig = lib.readFile (pkgs.stdenv.mkDerivation {
          inherit (pkgs) zsh;
          name = "tmux.conf";
          src = ./dotfiles/tmux.conf;
          buildInputs = with pkgs; [ makeWrapper ];
          phases = [ "installPhase" ];
          mandb = pkgs.man-db;
          installPhase = ''
            substituteAll $src $out
          '';
        });
      };

      neovim = {
        enable = true;
        vimAlias = true;
        viAlias = true;
        # defaultEditor = true;
        plugins = with pkgs.vimPlugins; [
          airline
          fugitive
          vim-nix
        ];
        extraConfig = builtins.readFile ./dotfiles/vim/.vimrc;
      };

      direnv = {
        enable = true;
        nix-direnv.enable = true;
      };
    };

    home.sessionVariables = {
    # TERM = "screen-256color";
    # EDITOR = "${editor}/bin/editor";
    # GIT_EDITOR = "${editor}/bin/editor";
    };

    home.packages = with pkgs; [
    ];

    home.stateVersion = "22.11";
    systemd.user.startServices = "sd-switch";
  };
}
