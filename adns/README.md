# Authoritative DNS

The `zones` subdirectory contains files for DNS zones served by us --
these can be manually modified to make changes to our DNS records.  
Don't forget to update the SOA! It's usually in the format of
`YYYYMMDDNN` where `NN` increases monotonically with every manual
change within the same day.

## Transfer keys

We accept runtime changes to the zones we serve via [RFC2136][].  
This requires us to set up TSIG keys so change requests are authenticated.

[RFC2136]: https://datatracker.ietf.org/doc/rfc2136/

We choose to manage separate TSIG keys for each zone we want to be able to update.  
We store YAML configuration fragments for knot configuration
containing these keys in `keys` -- one per file.

Generate new ones as follows:
```bash
gup keys/new.yaml
```

## ACME/dns-01

This configuration contains extra machinery to solve [ACME DNS-01
challenges][].

[ACME DNS-01 challenges]: https://letsencrypt.org/docs/challenge-types/#dns-01-challenge

For every domain that we would like to have certificates issued for by
Let's Encrypt we host a separate "sub"-zone with a very short validity
time.  
There we can have our ACME automation put a temporary TXT record (TTL
0 -- so no caching) containing the challenge given to us by Let's Encrypt.  
Doing so proves to them that we control the domain and they issue us
the certificate we want.

We have to manually set up NS records for all ACME subzones we wish to
have.  
We record `ns.uniworx.de` as the only authoritative nameserver for
these subzones.
We then don't have to wait for propagation ouf our TXT records to the
INWX secondaries.

"Hav[ing] our ACME automating put a temporary TXT record" is
accomplished via [RFC2136][], see above.
