{ config, pkgs, lib, ... }:

with lib;

let
  # Ex: blub.example.org -> org.example.blub
  reverseDomain = domain: concatStringsSep "." (reverseList (splitString "." domain));

  # Minimal, empty zonefile with a low TTL
  # Used exclusively to hold a temporary TXT record to solve ACME dns-1
  acmeChallengeZonefile = domain: pkgs.writeText "${reverseDomain "_acme-challenge.${domain}"}.soa" ''
    $ORIGIN _acme-challenge.${domain}.
    $TTL 3600
    @ IN SOA ns.uniworx.de. root.uniworx.de. 2023011801 10800 3600 604800 30

      IN NS  ns.uniworx.de.
  '';

  # Read ./keys directory at evaluation time
  # Ex: If ./keys/knot_local.acme and ./keys/uniworx.de.acme exist,
  #   then return:
  #     [ { path = ./keys/knot_local.acme; name = "knot_local.acme"; }
  #       { path = ./keys/uniworx.de.acme; name = "uniworx.de.acme"; }
  #     ]
  knotKeys = let
    dir = ./keys;
    toKeyInfo = name: v:
      if v == "regular" || v == "symlink"
      then { path = dir + "/${name}"; inherit name; }
      else null;
  in filter (v: v != null) (mapAttrsToList toKeyInfo (builtins.readDir dir));

  indentString = indentation: str: concatMapStringsSep "\n" (str: indentation + str) (splitString "\n" (removeSuffix "\n" str));

  # Generate a section for config.services.knot.extraConfig
  # corresponding to a single manually managed dns zone and
  # arbitrarily many _acme-challenge sub-zones
  mkZone = { domain
           , path ? (./zones + "/${reverseDomain domain}.soa")
           , acmeDomains ? [domain]
           , addACLs ? {}
           }: indentString "  " (let
             keys = acmeDomain: [
               (assert assertMsg (config.sops.secrets ? "${acmeDomain}_acme") "‘${acmeDomain}_acme’ is not present in `config.sops.secrets` -- is this a new ACME domain and you forget to generate the TSIG key? If so, run `gup tls/tsig_keys/${acmeDomain}`"; "${acmeDomain}_acme_acl")
             ] ++ (addACLs.${acmeDomain} or []);
           in ''
             - domain: ${domain}
               template: inwx_zone
               ${optionalString (acmeDomains != []) "acl: [local_acl, inwx_acl]"}
               file: ${path}
             ${concatMapStringsSep "\n" (acmeDomain: ''
               - domain: _acme-challenge.${acmeDomain}
                 template: acme_zone
                 acl: [${concatStringsSep ", " (keys acmeDomain)}]
                 file: ${acmeChallengeZonefile acmeDomain}
             '') acmeDomains}
           '');
in {
  config = {
    systemd.services.knot = {
      # Have systemd load all knotKeys into
      # /run/credentials/knot.service at runtime
      serviceConfig.LoadCredential = map ({name, ...}: "${name}:${config.sops.secrets.${name}.path}") knotKeys;
    };

    services.knot = {
      enable = true;
      # Have knot read all known key files from
      # /run/credentials/knot.service where systemd put them
      keyFiles = map ({name, ...}: "/run/credentials/knot.service/${name}") knotKeys;

      # Listen on our public IPs

      # Define some easier names for certain remote DNS servers,
      # including ourselves

      # INWX secondaries may request we transfer them our entire
      # zonefiles -- required for secondary DNS

      # When we automagically DNSSEC sign our _acme-challenge zones we
      # need the respective parent zones to contain a hash of the key
      # material used to sign the subordinate zone. We therefore allow
      # ourselves to push DS zone entries (which contain said hash)
      # for publication in the parent zones

      # (skip mod-rrl, mod-cookies, submission)

      # Define policies for automagically signing all of our DNS zones
      # for DNSSEC. Key management is entirely automated by knot

      # Templates for actual zones defined later. One pushes all
      # changes to INWX for secondary DNS, other is for
      # _acme-challenge with appropriate DNSSEC policy
      extraConfig = ''
        server:
          listen: 89.58.4.219@53
          listen: 2a03:4000:5e:e55::@53

        remote:
          - id: inwx_notify
            address: 2a0a:c980::53
            via: 2a03:4000:5e:e55::
          - id: recursive
            address: ::1@53
          - id: local
            address: 2a03:4000:5e:e55::@53
            key: knot_local_key

        acl:
          - id: inwx_acl
            address: [185.181.104.96, 2a0a:c980::53]
            action: transfer
          - id: local_acl
            key: knot_local_key
            action: update
            update-type: DS
        ${let
          toACMEACL = { name, ... }:
            if hasSuffix "_acme" name
            then indentString "  " ''
                - id: ${name}_acl
                  key: ${name}_key
                  action: update
              ''
            else null;
        in concatStringsSep "\n" (filter (v: v != null) (map toACMEACL knotKeys))}

        mod-rrl:
          - id: default
            rate-limit: 200
            slip: 2

        mod-cookies:
          - id: default
            secret-lifetime: 4h
            badcookie-slip: 1

        submission:
          - id: validating-resolver
            parent: recursive
            check-interval: 5m

        policy:
          - id: rsa2048
            algorithm: rsasha256
            ksk-size: 4096
            zsk-size: 2048
            nsec3: on
            nsec3-iterations: 0
            ksk-lifetime: 360d
            signing-threads: 2
            ksk-submission: validating-resolver
          - id: rsa2048_local-push
            algorithm: rsasha256
            ksk-size: 4096
            zsk-size: 2048
            nsec3: on
            nsec3-iterations: 0
            ksk-lifetime: 360d
            signing-threads: 2
            ksk-submission: validating-resolver
            cds-cdnskey-publish: double-ds
            propagation-delay: 0
            ds-push: [local]

        template:
          - id: default
            global-module: [mod-cookies/default, mod-rrl/default]
          - id: inwx_zone
            storage: /var/lib/knot
            zonefile-sync: -1
            zonefile-load: difference-no-serial
            serial-policy: dateserial
            journal-content: all
            semantic-checks: on
            dnssec-signing: on
            dnssec-policy: rsa2048
            notify: [inwx_notify]
            acl: [inwx_acl]
          - id: acme_zone
            storage: /var/lib/knot
            zonefile-sync: -1
            zonefile-load: difference-no-serial
            serial-policy: dateserial
            journal-content: all
            semantic-checks: on
            dnssec-signing: on
            dnssec-policy: rsa2048_local-push

        zone:
        ${concatMapStringsSep "\n" mkZone [
          { domain = "uniworx.de";
            acmeDomains = ["srv01.uniworx.de" "uniworx.de" "mailin.uniworx.de" "mailsub.uniworx.de" "imap.uniworx.de" "mta-sts.uniworx.de" "cloud.uniworx.de"];
          }
        ]}
      '';
    };

    sops.secrets = listToAttrs (map ({name, path}: nameValuePair name {
      format = "binary";
      sopsFile = path;
      restartUnits = [ "knot.service" ];
    }) knotKeys);
  };
}
