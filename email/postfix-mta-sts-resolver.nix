{ config, pkgs, lib, flakeInputs, ... }:

# Upstream server to query other servers' opinions on whether we need
# to use TLS to talk to them
#
# Cache is stored in bespoke redis instance, cache entries are
# proactively refreshed so they never expire and cause lag on outgoing
# mail

with lib;

let
  cfg = config.services.postfix-mta-sts-resolver;

  postfix-mta-sts-resolver = flakeInputs.mach-nix.lib.${config.nixpkgs.system}.buildPythonPackage {
    pname = "postfix-mta-sts-resolver";
    version = "1.1.5";
    src = pkgs.fetchurl {
      url = "https://github.com/Snawoot/postfix-mta-sts-resolver/archive/refs/tags/v1.1.5.tar.gz";
      sha256 = "sha256-AcWGxvXtzMiVTTWL3TmsY+tBI9vIGhRRifEjfpAGQ44=";
    };
    # extras = "redis,uvloop";
    ignoreDataOutdated = true;

    requirements = ''
      redis>=4.2.0rc1
      uvloop>=0.11.0
      aiodns>=1.1.1
      aiohttp>=3.4.4
      PyYAML>=3.12
    '';

    providers.cffi = "nixpkgs";
  };
in {
  options = {
    services.postfix-mta-sts-resolver = {
      enable = mkEnableOption "mta-sts-daemon";
      package = mkPackageOption pkgs "postfix-mta-sts-resolver";

      redis = mkEnableOption "redis cache" // { default = true; example = false; };
      proactive-policy-fetching = mkEnableOption "proactive policy fetching" // { default = true; example = false; };

      loglevel = mkOption {
        type = types.enum ["debug" "info" "warn" "error" "fatal"];
        default = "info";
      };

      settings = mkOption {
        type = types.attrs;
      };
    };
  };

  config = mkIf cfg.enable {
    services.postfix-mta-sts-resolver.settings = {
      path = "/run/postfix-mta-sts-resolver/map.sock";
      mode = 432; # 0o0660
    } // (optionalAttrs cfg.redis {
      cache = {
        type = "redis";
        options.url = "unix://${toString config.services.redis.servers.postfix-mta-sts-resolver.unixSocket}";
      };
    })
    // (optionalAttrs cfg.proactive-policy-fetching {
      proactive_policy_fetching.enabled = true;
    });

    services.redis.servers.postfix-mta-sts-resolver = mkIf cfg.redis {
      enable = true;
    };

    users.users.postfix-mta-sts-resolver = {
      isSystemUser = true;
      group = "postfix-mta-sts-resolver";
    };
    users.groups.postfix-mta-sts-resolver = {
      members = ["postfix"];
    };

    systemd.services."postfix-mta-sts-resolver" = {
      wantedBy = ["postfix.service"];
      before = ["postfix.service"];

      wants = mkIf cfg.redis [ "redis-postfix-mta-sts-resolver.service" ];
      after = mkIf cfg.redis [ "redis-postfix-mta-sts-resolver.service" ];

      serviceConfig = {
        Type = "notify";
        ExecStart = "${postfix-mta-sts-resolver}/bin/mta-sts-daemon -v ${cfg.loglevel} -c ${pkgs.writeText "mta-sts-daemon.yml" (generators.toYAML {} cfg.settings)}";
        Restart = "always";
        KillMode = "process";
        TimeoutStartSec = 10;
        TimeoutStopSec = 30;

        RuntimeDirectory = "postfix-mta-sts-resolver";

        User = "postfix-mta-sts-resolver";
        Group = "postfix-mta-sts-resolver";
        SupplementaryGroups = mkIf cfg.redis config.services.redis.servers.postfix-mta-sts-resolver.user;

        RemoveIPC = true;
        PrivateTmp = true;
        NoNewPrivileges = true;
        RestrictSUIDSGID = true;
        ProtectSystem = "strict";
        ProtectHome = "read-only";
        ReadWritePaths = mkIf cfg.redis ["/run/redis-postfix-mta-sts-resolver"];
      };
    };
  };
}
