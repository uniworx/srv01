from setuptools import setup, find_packages

setup(
    name = 'ccert-sender-policy-server',
    version = '0.0.0',
    packages = ['ccert_sender_policy_server'],
    entry_points = {
        'console_scripts': [
            'ccert-sender-policy-server=ccert_sender_policy_server.__main__:main'
        ],
    },
)
