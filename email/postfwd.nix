{ config, lib, pkgs, ... }:

# Upstream application level "firewall" for making policy decisions on
# email passing through postfix
#
# We use it to ratelimit outgoing mail by user

with lib;

let
  cfg = config.services.postfwd;

  postfwd = let
    deps = with pkgs.perlPackages; [NetDNS NetServer IOMultiplex NetAddrIP NetCIDRLite DigestMD5 TimeHiRes Storable];
  in pkgs.stdenv.mkDerivation rec {
    pname = "postfwd";
    version = "2.03";
    src = pkgs.fetchurl {
      url = "https://github.com/postfwd/postfwd/archive/refs/tags/v2.03.tar.gz";
      sha256 = "sha256-mMKXzeqg2PfXkvGL7qugOelm/I2fZnUidq6/ugXDHa0=";
    };

    nativeBuildInputs = with pkgs; [ makeWrapper ];
    propagatedBuildInputs = [pkgs.perlPackages.perl] ++ deps;

    buildPhase = ''
      runHook preBuild

      substituteInPlace sbin/postfwd3 \
        --replace "/usr/bin/perl -T" "/usr/bin/perl"

      runHook postBuild
    '';

    installPhase = ''
      runHook preInstall

      mkdir -p $out/bin
      cp -t $out/bin sbin/postfwd3

      wrapProgram $out/bin/postfwd3 \
        --prefix PERL5LIB : ${pkgs.perlPackages.makePerlPath deps}

      runHook postInstall
    '';
  };
in {
  options = {
    services.postfwd = with types; {
      enable = mkEnableOption "postfwd3 - postfix firewall daemon";

      rules = mkOption {
        type = lines;
        default = "";
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.postfwd = {
      description = "postfwd3 - postfix firewall daemon";
      wantedBy = ["multi-user.target"];
      before = ["postfix.service"];

      serviceConfig = {
        Type = "forking";

        ExecStart = "${postfwd}/bin/postfwd3 ${escapeShellArgs [
          "-vv"
          "--daemon" "--user" "postfwd" "--group" "postfwd"
          "--pidfile" "/run/postfwd3/postfwd3.pid"
          "--proto" "unix"
          "--port" "/run/postfwd3/postfwd3.sock"
          "--save_rates" "/var/lib/postfwd/rates"
          "--save_groups" "/var/lib/postfwd/groups"
          "--summary" "3600"
          "--cache" "600"
          "--cache_proto" "unix"
          "--cache_port" "/run/postfwd3/cache.sock"
          "--file" (pkgs.writeText "postfwd3-rules" cfg.rules)
        ]}";
        PIDFile = "/run/postfwd3/postfwd3.pid";

        Restart = "always";
        RestartSec = 5;
        TimeoutSec = 10;

        RuntimeDirectory = ["postfwd3"];
        StateDirectory = ["postfwd"];

        DynamicUser = true;
        ProtectSystem = "strict";
        ProtectHome = true;
        SystemCallFilter = ["@system-service" "~@resources @obsolete"];
        NoNewPrivileges = true;
        ProtectKernelTunables = true;
        ProtectKernelModules = true;
        ProtectKernelLogs = true;
        ProtectControlGroups = true;
        MemoryDenyWriteExecute = true;
        RestrictSUIDSGID = true;
        KeyringMode = "private";
        ProtectClock = true;
        RestrictRealtime = true;
        PrivateDevices = true;
        PrivateTmp = true;
        ProtectHostname = true;
        RestrictNamespaces = true;
        CapabilityBoundingSet = "";
        RestrictAddressFamilies = ["AF_UNIX"];
        PrivateNetwork = true;
        PrivateUsers = true;
        SystemCallArchitectures = "native";
        LockPersonality = true;
        ProtectProc = "invisible";
        ProcSubset = "pid";
        DevicePolicy = "closed";
        IPAddressDeny = "any";
      };
    };
  };
}
