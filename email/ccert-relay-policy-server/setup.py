from setuptools import setup, find_packages

setup(
    name = 'ccert-relay-policy-server',
    version = '0.0.0',
    packages = ['ccert_relay_policy_server'],
    entry_points = {
        'console_scripts': [
            'ccert-relay-policy-server=ccert_relay_policy_server.__main__:main'
        ],
    },
)
