from systemd.daemon import listen_fds
from sdnotify import SystemdNotifier
from socketserver import StreamRequestHandler, ThreadingMixIn
from systemd_socketserver import SystemdSocketServer
import sys
from threading import Thread
import json

import argparse
import logging

from pathlib import Path


class PolicyHandler(StreamRequestHandler):
    def handle(self):
        logger.debug('Handling new connection...')

        self.args = dict()

        line = None
        while line := self.rfile.readline().removesuffix(b'\n'):
            if b'=' not in line:
                break

            key, val = line.split(sep=b'=', maxsplit=1)
            self.args[key.decode()] = val.decode()

        logger.info('Connection parameters: %s', self.args)

        action = 'DUNNO'
        if self.args['ccert_subject'] in self.server.cn_whitelist:
            action = 'OK'

        logger.info('Reached verdict: %s', {'action': action})
        self.wfile.write(f'action={action}\n\n'.encode())

class ThreadedSystemdSocketServer(ThreadingMixIn, SystemdSocketServer):
    def __init__(self, fd, RequestHandlerClass, cn_whitelist):
        super().__init__(fd, RequestHandlerClass)

        self.cn_whitelist = cn_whitelist

def main():
    global logger
    logger = logging.getLogger(__name__)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter( logging.Formatter('[%(levelname)s](%(name)s): %(message)s') )
    if sys.stderr.isatty():
        console_handler.setFormatter( logging.Formatter('%(asctime)s [%(levelname)s](%(name)s): %(message)s') )
    logger.addHandler(console_handler)
    logger.setLevel(logging.DEBUG)

    # log uncaught exceptions
    def log_exceptions(type, value, tb):
        global logger

        logger.error(value)
        sys.__excepthook__(type, value, tb) # calls default excepthook

    sys.excepthook = log_exceptions

    parser = argparse.ArgumentParser()
    parser.add_argument('cn_whitelist', type=Path)
    args = parser.parse_args()

    cn_whitelist = None
    with args.cn_whitelist.open('r') as fh:
        cn_whitelist = set(json.load(fh))

    fds = listen_fds()
    servers = [ThreadedSystemdSocketServer(fd, PolicyHandler, cn_whitelist) for fd in fds]

    if servers:
        for server in servers:
            Thread(name=f'Server for fd{server.fileno()}', target=server.serve_forever).start()
    else:
        return 2

    SystemdNotifier().notify('READY=1')

    return 0

if __name__ == '__main__':
    sys.exit(main())
