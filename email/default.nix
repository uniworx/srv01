{ config, pkgs, lib, flakeInputs, ... }:

with lib;

let
  # Collection of shell scripts to execute from sieve scripts
  #
  # Currently provides for passing information about emails manually
  # marked as spam/not-spam to rspamd
  dovecotSievePipeBin = pkgs.stdenv.mkDerivation {
    name = "dovecot-sieve-pipe-bin";
    src = ./dovecot-pipe-bin;
    buildInputs = with pkgs; [ makeWrapper coreutils bash rspamd ];
    buildCommand = ''
      mkdir -p $out/pipe/bin
      cp $src/* $out/pipe/bin/
      chmod a+x $out/pipe/bin/*
      patchShebangs $out/pipe/bin

      for file in $out/pipe/bin/*; do
        wrapProgram $file \
          --set PATH "${pkgs.coreutils}/bin:${pkgs.rspamd}/bin"
      done
    '';
  };

  # Small python script that is communicated with by postfix and then
  # consults the database to determine whether a given TLS-certificate
  # CN may use a given SMTP envelope from address
  ccert-sender-policy-server = flakeInputs.mach-nix.lib.${config.nixpkgs.system}.buildPythonPackage {
    src = ./ccert-sender-policy-server;
    pname = "ccert-sender-policy-server";
    version = "0.0.0";

    python = "python39";
    ignoreDataOutdated = true;

    requirements = ''
      sdnotify
      systemd-socketserver
      psycopg >=3.0.0
      psycopg-pool >=3.0.0
      psycopg-binary >=3.0.0
    '';

    overridesPre = [
      (self: super: { systemd-python = super.systemd.overrideAttrs (oldAttrs: { pname = "systemd-python"; }); })
    ];
  };

  # Small python script that checks whether a given TLS-certificate CN
  # is in a JSON encoded whitelist
  #
  # Used to determine whether a given TLS-certificate may be used to
  # relay mail arbitrarily by e.g. NextCloud
  ccert-relay-policy-server = flakeInputs.mach-nix.lib.${config.nixpkgs.system}.buildPythonPackage {
    src = ./ccert-relay-policy-server;
    pname = "ccert-relay-policy-server";
    version = "0.0.0";

    python = "python39";
    ignoreDataOutdated = true;

    requirements = ''
      sdnotify
      systemd-socketserver
    '';

    overridesPre = [
      (self: super: { systemd-python = super.systemd.overrideAttrs (oldAttrs: { pname = "systemd-python"; }); })
    ];
  };

  # List of domains for which to provide email services
  emailDomains = ["uniworx.de"];
in {
  imports = [
    ./postfix-mta-sts-resolver.nix ./postfwd.nix
  ];

  config = {
    # Configure Postfix and Dovecot2 to support accessing PostgreSQL
    nixpkgs.overlays = [
      (final: prev: {
        postfix = prev.postfix.override {
          withLDAP = false;
          withPgSQL = true;
        };
        dovecot = prev.dovecot.override {
          withSQLite = false;
          withPgSQL = true;
        };
      })
    ];

    # SMTP server
    services.postfix = {
      enable = true;
      enableSmtp = false; # We enable SMTP service manually later
      hostname = "srv01.uniworx.de";

      # We configure almost everything manually
      recipientDelimiter = "";
      setSendmail = true;
      postmasterAlias = ""; rootAlias = ""; extraAliases = "";
      destination = [];
      networks = [];

      # Secrets provided to the service via systemd, see `LoadCredential`
      sslCert = "/run/credentials/postfix.service/srv01.uniworx.de.pem";
      sslKey = "/run/credentials/postfix.service/srv01.uniworx.de.key.pem";

      config = {
        # Configuration specified here is the default and used for
        # normal SMTP service on port 25 (incoming mail from outside
        # and relaying from e.g. NextCloud)

        # TLS client certificates for incoming mail are optional in
        # principle, relaying will require one
        smtpd_tls_security_level = "may";

        #the dh params
        smtpd_tls_dh1024_param_file = toString config.security.dhparams.params."postfix-1024".path;
        smtpd_tls_dh512_param_file = toString config.security.dhparams.params."postfix-512".path;
        #enable ECDH
        smtpd_tls_eecdh_grade = "strong";
        #enabled SSL protocols, don't allow SSLv2 and SSLv3
        smtpd_tls_protocols = ["!SSLv2" "!SSLv3" "!TLSv1" "!TLSv1.1"];
        smtpd_tls_mandatory_protocols = ["!SSLv2" "!SSLv3" "!TLSv1" "!TLSv1.1"];
        #allowed ciphers for smtpd_tls_security_level=encrypt
        smtpd_tls_mandatory_ciphers = "medium";
        #allowed ciphers for smtpd_tls_security_level=may
        #smtpd_tls_ciphers = high
        #enforce the server cipher preference
        tls_preempt_cipherlist = true;
        #disable following ciphers for smtpd_tls_security_level=encrypt
        smtpd_tls_mandatory_exclude_ciphers = ["aNULL" "MD5" "DES" "ADH" "RC4" "PSD" "SRP" "3DES" "eNULL"];
        #disable following ciphers for smtpd_tls_security_level=may
        smtpd_tls_exclude_ciphers = ["aNULL" "MD5" "DES" "ADH" "RC4" "PSD" "SRP" "3DES" "eNULL"];
        #enable TLS logging to see the ciphers for inbound connections
        smtpd_tls_loglevel = "1";
        #enable TLS logging to see the ciphers for outbound connections
        smtp_tls_loglevel = "1";
        tls_medium_cipherlist = "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384";

        # Include information about TLS client certificate in
        # `Received`-Header in all mails passing through this server
        # -- for debugging and accountability
        smtpd_tls_received_header = true;

        # We ask for (optionally) and accept client certificates
        # signed by our CA
        smtpd_tls_ask_ccert = true;
        smtpd_tls_CAfile = toString ./ca/ca.crt;

        # Outgoing TLS may use DANE to retrieve public key material
        # from (authenticated) DNS
        #
        # If that is not available fallback to accepting any server
        # certificate
        smtp_tls_security_level = "dane";
        smtp_dns_support_level = "dnssec";

        # Keep outgoing connections open for a while
        smtp_tls_connection_reuse = true;

        # We use separate certificates for all domains we serve
        # Modern TLS clients indicate to us which domain they want to
        # talk with and we can use that information to select the
        # proper certificate/private key files
        tls_server_sni_maps = ''texthash:${pkgs.writeText "sni" (
          concatMapStringsSep "\n\n" (domain:
            concatMapStringsSep "\n" (subdomain: "${subdomain} /run/credentials/postfix.service/${removePrefix "." subdomain}.full.pem")
              [domain "mailin.${domain}" "mailsub.${domain}" ".${domain}"]
          ) emailDomains
        )}'';

        # If external mailservers have MTA-STS set up we want to
        # respect that
        # `postfix-mta-sts-resolver` queries the remote MTA-STS config
        # (with caching)
        # If MTA-STS turns out to be configured we insist on using TLS
        # and refuse to submit emails in the clear
        # That's a bit better than just using opportunistic encryption
        # and very robust in combination with DNSSEC
        smtp_tls_policy_maps = "socketmap:unix:${config.services.postfix-mta-sts-resolver.settings.path}:postfix";

        # There are no local recipients; virtual mailboxes only
        local_recipient_maps = "";

        # 10 GiB
        message_size_limit = "10737418240";
        # 10 GiB
        mailbox_size_limit = "10737418240";

        # Defer some policy checks until later in the transaction to
        # provide more information to policy services
        smtpd_delay_reject = true;
        smtpd_helo_required = true;
        smtpd_helo_restrictions = "permit";

        # Incoming email is checked for whether the specified
        # recipients fulfill the following criteria
        #
        # Criteria are evaluated in order
        smtpd_recipient_restrictions = [
          "reject_unauth_pipelining" # Commands sent by client need to follow proper order; out of order commands are common among spammers
          "reject_non_fqdn_recipient" # Recipients need to be proper email addresses, not fragments
          "reject_unknown_recipient_domain" # Domain of specified recipient needs to at least exist
          "check_recipient_access pgsql:${pkgs.writeText "check_recipient_access.cf" ''
            hosts = postgresql:///email
            dbname = email
            query = SELECT action FROM virtual_mailbox_access WHERE lookup = '%s'
          ''}" # If recipient address is local, check in database whether we deliver to it
          "check_policy_service unix:/run/postfix-ccert-relay-policy.sock" # If TLS client certificate is used and in whitelist, accept anything
          "reject_non_fqdn_helo_hostname" # Require client to specify proper (i.e. incl. domain) hostname for themselves
          "reject_invalid_helo_hostname" # ditto
          "reject_unauth_destination" # Require recipient address to be local to this server
          "reject_unknown_recipient_domain" # ???
          "reject_unverified_recipient" # Check if recipient server(s) (local dovecot over LMTP) claim(s) that all recipients' addresses are deliverable (mailbox is not full, user exists in DB, etc.)
        ];

        # Caching and responses for `reject_unverified_recipient`
        unverified_recipient_reject_code = "550";
        unverified_recipient_reject_reason = "Recipient address lookup failed";
        address_verify_map = "internal:address_verify_map";
        address_verify_positive_expire_time = "1h";
        address_verify_positive_refresh_time = "15m";
        address_verify_negative_expire_time = "15s";
        address_verify_negative_refresh_time = "5s";
        address_verify_cache_cleanup_interval = "5s";
        address_verify_poll_delay = "1s";

        # Additional recipient restrictions for when we are not the
        # final destination
        smtpd_relay_restrictions = [
          "check_policy_service unix:/run/postfix-ccert-relay-policy.sock" # TLS client certificates on our whitelist may relay anywhere
          "reject_unauth_destination" # Everyone else may only relay to `relay_domains`, which should be empty
        ];

        propagate_unmatched_extensions = ["canonical" "virtual" "alias"]; # ???

        # Nobody is allowed to use XVERP
        smtpd_authorized_verp_clients = "";
        authorized_verp_clients = "";

        # Nobody may bypass SMTP command ratelimits
        smtpd_client_event_limit_exceptions = "";

        # Pass all mail through opendkim and rspamd for signing/verification/spam-marking
        milter_default_action = "accept";
        smtpd_milters = [config.services.opendkim.socket "local:/run/rspamd/rspamd-milter.sock"];
        non_smtpd_milters = [config.services.opendkim.socket "local:/run/rspamd/rspamd-milter.sock"];

        # No aliases, only virtual mailboxes
        alias_maps = "";

        # Much smaller lifetimes for various queues; more responsive mail (non)delivery
        queue_run_delay = "10s";
        minimal_backoff_time = "1m";
        maximal_backoff_time = "10m";
        maximal_queue_lifetime = "100m";
        bounce_queue_lifetime = "20m";

        smtpd_discard_ehlo_keyword_address_maps = "cidr:${pkgs.writeText "esmtp_access" ''
          # Allow DSN requests from local subnet only
          192.168.0.0/16      silent-discard
          172.16.0.0/12       silent-discard
          10.0.0.0/8          silent-discard
          0.0.0.0/0           silent-discard, dsn
          fd00::/8            silent-discard
          ::/0                silent-discard, dsn
        ''}";

        # Replace non-local outgoing addresses with local temporary
        # alias and resolve back for incoming mail (SRS)
        sender_canonical_maps = "tcp:localhost:${toString config.services.postsrsd.forwardPort}";
        sender_canonical_classes = "envelope_sender";
        recipient_canonical_maps = "tcp:localhost:${toString config.services.postsrsd.reversePort}";
        recipient_canonical_classes = ["envelope_recipient" "header_recipient"];

        # Which domains contain virtual mailboxes? Ask the database
        virtual_mailbox_domains = ''pgsql:${pkgs.writeText "virtual_mailbox_domains.cf" ''
          hosts = postgresql:///email
          dbname = email
          query = SELECT 1 FROM virtual_mailbox_domain WHERE domain = '%s'
        ''}'';
        # Which virtual mailboxes exist? Ask the database
        virtual_mailbox_maps = ''pgsql:${pkgs.writeText "virtual_mailbox_maps.cf" ''
          hosts = postgresql:///email
          dbname = email
          query = SELECT 1 FROM virtual_mailbox_mapping WHERE lookup = '%s' OR (lookup = regexp_replace('%s', '\+[^@]*@', '@') AND NOT EXISTS (SELECT 1 FROM virtual_mailbox_mapping WHERE lookup = '%s'))
        ''}'';
        # At most one recipient at a time for dovecot over LMTP (???)
        dvlmtp_destination_recipient_limit = "1";
        # Emails to virtual recipients go to dovecot over LMTP
        virtual_transport = "dvlmtp:unix:/run/dovecot-lmtp";
        # Don't try to handle UTF-8 in any fancy way; just pass it through unchanged
        # Dovecot requires this
        smtputf8_enable = false;

        # Only select users may use `sendmail`
        authorized_submit_users = "inline:{ root= postfwd= }";

        # We delay processing incoming connections for a bit
        # (postscreen), if the client does not cooperate, just drop
        # the connection
        postscreen_access_list = "cidr:${pkgs.writeText "postscreen-access" ''
          192.168.0.0/16      permit
          172.16.0.0/12       permit
          10.0.0.0/8          permit
          fd00::/8            permit
        ''}";
        postscreen_denylist_action = "drop";
        postscreen_greet_action = "enforce";
      };
      masterConfig = {
        # Transport encrypted mail submission by end-users (port 465)
        smtps = {
          type = "inet";
          private = false;
          command = "smtpd";
          args = [
            # Don't negotiate whether to use TLS (STARTTLS)
            # Instead require client to set up TLS before issuing any command (transport encryption)
            "-o" "smtpd_tls_wrappermode=yes"
            "-o" "smtpd_tls_security_level=encrypt"

            # Harden
            "-o" "{smtpd_tls_mandatory_protocols = !SSLv2, !SSLv3, !TLSv1, !TLSv1.1, !TLSv1.2}"
            "-o" "{smtpd_tls_protocols = !SSLv2, !SSLv3, !TLSv1, !TLSv1.1, !TLSv1.2}"
            "-o" "smtpd_tls_mandatory_ciphers=high"
            "-o" "smtpd_tls_dh1024_param_file=${toString config.security.dhparams.params."postfix-smtps-1024".path}"
            "-o" "smtpd_tls_dh512_param_file=${toString config.security.dhparams.params."postfix-smtps-512".path}"
            "-o" "{tls_eecdh_auto_curves = X25519 X448}"

            # Require TLS client certificates
            "-o" "smtpd_tls_ask_ccert=yes"
            "-o" "smtpd_tls_req_ccert=yes"

            # Don't append TLS information to Received headers
            "-o" "smtpd_tls_received_header=no"

            # Pass email through a special cleanup server to strip some further identifieing information
            "-o" "cleanup_service_name=subcleanup"

            # All TLS client certificates with a valid CA signature are fine
            "-o" "smtpd_client_restrictions=permit_tls_all_clientcerts,reject"
            "-o" "smtpd_relay_restrictions=permit_tls_all_clientcerts,reject"

            # Check ratelimiting
            "-o" "{smtpd_data_restrictions = check_policy_service unix:/run/postfwd3/postfwd3.sock}"

            # Check if smtp envelope from is allowed for TLS client CN
            # used by consulting policy server, which in turn consults
            # the database
            # Also check if smtp envelope from is known to be a
            # deliverable email address
            "-o" "{smtpd_sender_restrictions = reject_unknown_sender_domain,reject_unverified_sender,check_policy_service unix:/run/postfix-ccert-sender-policy.sock}"
            "-o" "unverified_sender_reject_code=550"
            "-o" "unverified_sender_reject_reason={Sender address rejected: undeliverable address}"

            # If local recipient (i.e.: mail from local sender to local recipient), allow some stuff that we wouldn't allow on receipt
            # E.g. improper client hostname
            "-o" ''{smtpd_recipient_restrictions=reject_unauth_pipelining,reject_non_fqdn_recipient,reject_unknown_recipient_domain,check_recipient_access pgsql:${pkgs.writeText "check_recipient_access.cf" ''
            hosts = postgresql:///email
            dbname = email
            query = SELECT action FROM virtual_mailbox_access WHERE lookup = '%s' OR (lookup = regexp_replace('%s', '\+[^@]*@', '@') AND NOT EXISTS (SELECT 1 FROM virtual_mailbox_access WHERE lookup = '%s'))
          ''},permit_tls_all_clientcerts,reject}''

            # Pass mail through opendkim for signing
            "-o" "milter_macro_daemon_name=srv01.uniworx.de"
            "-o" ''smtpd_milters=${config.services.opendkim.socket}''
          ];
        };
        subcleanup = {
          command = "cleanup";
          private = false;
          maxproc = 0;
          args = [
            # Remove client IP address from Received-header
            "-o" "header_checks=pcre:${pkgs.writeText "header_checks_submission" ''
              /^Received: from [^ ]+ \([^ ]+ [^ ]+\)\s+(.*)$/ REPLACE Received: $1
            ''}"
          ];
        };
        dvlmtp = {
          command = "lmtp";
          args = [
            "flags=DORX"
          ];
        };
        smtp_pass = {
          name = "smtpd";
          type = "pass";
          command = "smtpd";
        };
        postscreen = {
          name = "smtp";
          type = "inet";
          private = false;
          command = "postscreen";
          maxproc = 1;
        };
        smtp = {};
        relay = {
          command = "smtp";
          args = [ "-o" "smtp_fallback_relay=" ];
        };
        tlsproxy = {
          maxproc = 0;
        };
        dnsblog = {};
      };
    };

    # Support for SRS on all our domains
    services.postsrsd = {
      enable = true;
      domain = "srv01.uniworx.de";
      separator = "+";
      excludeDomains = [ "srv01.uniworx.de"
                       ] ++ concatMap (domain: [".${domain}" domain]) emailDomains;
    };

    # Support for DKIM on all our domains (don't forget DNS!)
    services.opendkim = {
      enable = true;
      user = "postfix"; group = "postfix";
      socket = "local:/run/opendkim/opendkim.sock";
      domains = ''csl:${concatStringsSep "," (["srv01.uniworx.de"] ++ emailDomains)}'';
      selector = "srv01";
      configFile = builtins.toFile "opendkim.conf" ''
        Syslog true
        MTA srv01.uniworx.de
        MTACommand ${config.security.wrapperDir}/sendmail
        LogResults true
      '';
    };

    # Spam fiter
    #
    # Configuration is set up for adaptive learning but otherwise very
    # conservative and minimalistic
    services.rspamd = {
      enable = true;
      workers = {
        controller = {
          type = "controller";
          count = 1;
          bindSockets = [
            { mode = "0660";
              socket = "/run/rspamd/worker-controller.sock";
              owner = config.services.rspamd.user;
              group = config.services.rspamd.group;
            }
          ];
          includes = [];
          extraConfig = ''
            static_dir = "''${WWWDIR}"; # Serve the web UI static assets
          '';
        };
        external = {
          type = "rspamd_proxy";
          bindSockets = [
            { mode = "0660";
              socket = "/run/rspamd/rspamd-milter.sock";
              owner = config.services.rspamd.user;
              group = config.services.rspamd.group;
            }
          ];
          extraConfig = ''
            milter = yes;
            timeout = 120s;

            upstream "local" {
              default = yes;
              self_scan = yes;
            }
          '';
        };
      };
      locals = {
        "milter_headers.conf".text = ''
          use = ["authentication-results", "x-spamd-result", "x-rspamd-queue-id", "x-rspamd-server", "x-spam-level", "x-spam-status"];
          extended_headers_rcpt = [];
        '';
        "actions.conf".text = ''
          reject = 15;
          add_header = 10;
          greylist = 5;
        '';
        "groups.conf".text = ''
          symbols {
            "BAYES_SPAM" {
              weight = 2.0;
            }
          }
        '';
        "dmarc.conf".text = ''
          reporting = true;
          send_reports = true;
          report_settings {
            org_name = "uniworx.de";
            domain = "uniworx.de";
            email = "postmaster@uniworx.de";
          }
        '';
        "redis.conf".text = ''
          servers = "${config.services.redis.servers.rspamd.unixSocket}";
        '';
        "dkim_signing.conf".text = "enabled = false;";
        "neural.conf".text = "enabled = false;";
        "classifier-bayes.conf".text = ''
          enable = true;
          expire = 8640000;
          new_schema = true;
          backend = "redis";
          per_user = true;
          min_learns = 0;

          autolearn = [0, 10];

          statfile {
              symbol = "BAYES_HAM";
              spam = false;
          }
          statfile {
              symbol = "BAYES_SPAM";
              spam = true;
          }
        '';
        # "redirectors.inc".text = ''
        #   visit.creeper.host
        # '';
      };
    };

    # Store spam filter state in bespoke redis instance
    users.groups.${config.services.rspamd.group}.members = [ config.services.postfix.user "dovecot2" ];
    services.redis.servers.rspamd.enable = true;
    users.groups.${config.services.redis.servers.rspamd.user}.members = [ config.services.rspamd.user ];

    # IMAP/Sieve/Managesieve server
    services.dovecot2 = {
      enable = true;

      # No local users
      enablePAM = false;

      # Secrets provided to the service via systemd, see `LoadCredential`
      sslServerCert = "/run/credentials/dovecot2.service/srv01.uniworx.de.pem";
      sslServerKey = "/run/credentials/dovecot2.service/srv01.uniworx.de.key.pem";

      # Clients authenticate themselves via TLS client certificate; this is the CA
      sslCACert = toString ./ca/ca.crt;

      # Where to store mail and associated fulltext indices
      mailLocation = "maildir:/var/lib/mail/%u/maildir:UTF-8:INDEX=/var/lib/dovecot/indices/%u";

      modules = with pkgs; [
        dovecot_pigeonhole # server-side filtering
        dovecot_fts_xapian # proper fulltext indexing
      ];
      mailPlugins.globally.enable = [ "fts" "fts_xapian" ]; # fulltext search available everywhere

      # *In addition* to IMAP
      protocols = [
        "lmtp" # Incoming mail from postfix
        "sieve" # Allow users to modify their own server-side filtering rules
      ];

      extraConfig = let
        # How to communicate with PostgreSQL?
        dovecotSqlConf = pkgs.writeText "dovecot-sql.conf" ''
          driver = pgsql
          connect = dbname=email
          password_query = SELECT NULL as password, 'Y' as nopassword, "user", quota_rule, 'dovecot2' as uid, 'dovecot2' as gid FROM imap_user WHERE "user" = '%n'
          user_query = SELECT "user", quota_rule, 'dovecot2' as uid, 'dovecot2' as gid FROM imap_user WHERE "user" = '%n'
          iterate_query = SELECT "user" FROM imap_user
        '';
      in ''
        mail_home = /var/lib/mail/%u

        mail_plugins = $mail_plugins quota

        first_valid_uid = ${toString config.users.users.dovecot2.uid}
        last_valid_uid = ${toString config.users.users.dovecot2.uid}
        first_valid_gid = ${toString config.users.groups.dovecot2.gid}
        last_valid_gid = ${toString config.users.groups.dovecot2.gid}

        ${concatMapStringsSep "\n\n" (domain:
          concatMapStringsSep "\n" (subdomain: ''
            local_name ${subdomain} {
              ssl_cert = </run/credentials/dovecot2.service/${subdomain}.pem
              ssl_key = </run/credentials/dovecot2.service/${subdomain}.key.pem
            }
          '') ["imap.${domain}" domain]
        ) emailDomains}

        ssl_require_crl = no
        ssl_verify_client_cert = yes

        ssl_min_protocol = TLSv1.2
        ssl_cipher_list = ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
        ssl_prefer_server_ciphers = no

        auth_ssl_username_from_cert = yes
        ssl_cert_username_field = commonName
        auth_mechanisms = external

        auth_verbose = yes
        verbose_ssl = yes
        auth_debug = yes

        service auth {
          user = dovecot2
        }
        service auth-worker {
          user = dovecot2
        }

        userdb {
          driver = prefetch
        }
        userdb {
          driver = sql
          args = ${dovecotSqlConf}
        }
        passdb {
          driver = sql
          args = ${dovecotSqlConf}
        }

        protocol lmtp {
          userdb {
            driver = sql
            args = ${pkgs.writeText "dovecot-sql.conf" ''
              driver = pgsql
              connect = dbname=email
              user_query = SELECT DISTINCT ON (extension IS NULL, local IS NULL) "user", quota_rule, 'dovecot2' as uid, 'dovecot2' as gid FROM lmtp_mapping WHERE CASE WHEN extension IS NOT NULL AND local IS NOT NULL THEN ('%n' :: citext) = local || '+' || extension AND domain = ('%d' :: citext) WHEN local IS NOT NULL THEN (local = ('%n' :: citext) OR ('%n' :: citext) ILIKE local || '+%%') AND domain = ('%d' :: citext) WHEN extension IS NOT NULL THEN ('%n' :: citext) ILIKE '%%+' || extension AND domain = ('%d' :: citext) ELSE domain = ('%d' :: citext) END ORDER BY (extension IS NULL) ASC, (local IS NULL) ASC
            ''}

            skip = never
            result_failure = return-fail
            result_internalfail = return-fail
          }

          mail_plugins = $mail_plugins sieve
        }

        mailbox_list_index = yes
        postmaster_address = postmaster@uniworx.de
        recipient_delimiter =
        auth_username_chars = abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890.-+_@

        service lmtp {
          vsz_limit = 1G

          unix_listener /run/dovecot-lmtp {
            mode = 0600
            user = postfix
            group = postfix
          }
        }

        namespace inbox {
          separator = /
          inbox = yes
          prefix =

          mailbox Trash {
            auto = no
            special_use = \Trash
          }
          mailbox Junk {
            auto = no
            special_use = \Junk
          }
          mailbox Drafts {
            auto = no
            special_use = \Drafts
          }
          mailbox Sent {
            auto = subscribe
            special_use = \Sent
          }
          mailbox "Sent Messages" {
            auto = no
            special_use = \Sent
          }
        }

        plugin {
          quota = count
          quota_rule = *:storage=1GB
          quota_rule2 = Trash:storage=+10%%
          quota_status_overquota = "552 5.2.2 Mailbox is full"
          quota_status_success = DUNNO
          quota_status_nouser = DUNNO
          quota_grace = 10%%
          quota_max_mail_size = ${config.services.postfix.config.message_size_limit}
          quota_vsizes = yes
        }

        protocol imap {
          mail_max_userip_connections = 50
          mail_plugins = $mail_plugins imap_quota imap_sieve
        }

        service imap-login {
          inet_listener imap {
            port = 0
          }
        }

        service managesieve-login {
          inet_listener sieve {
            port = 4190
          }
        }

        plugin {
          sieve_plugins = sieve_imapsieve sieve_extprograms
          sieve = file:~/sieve;active=~/dovecot.sieve
          sieve_redirect_envelope_from = orig_recipient
          sieve_before = /etc/dovecot/sieve_before.d

          sieve_global_extensions = +vnd.dovecot.pipe +vnd.dovecot.environment
          sieve_pipe_bin_dir = ${dovecotSievePipeBin}/pipe/bin

          imapsieve_mailbox1_name = *
          imapsieve_mailbox1_causes = FLAG
          imapsieve_mailbox1_before = /etc/dovecot/sieve_flag.d/learn-junk.sieve
        }

        plugin {
          plugin = fts fts_xapian
          fts = xapian
          fts_xapian = partial=2 full=20 attachments=1 verbose=1

          fts_autoindex = yes

          fts_enforced = no
        }

        service indexer-worker {
          vsz_limit = ${toString (1024 * 1024 * 1024)}
        }
      '';
    };

    # Periodically optimize fulltext indices
    systemd.services.dovecot-fts-xapian-optimize = {
      description = "Optimize dovecot indices for fts_xapian";
      requisite = [ "dovecot2.service" ];
      after = [ "dovecot2.service" ];
      startAt = "*-*-* 22:00:00 Europe/Berlin";
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${pkgs.dovecot}/bin/doveadm fts optimize -A";
        PrivateDevices = true;
        PrivateNetwork = true;
        ProtectKernelTunables = true;
        ProtectKernelModules = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectSystem = true;
        PrivateTmp = true;
      };
    };
    systemd.timers.dovecot-fts-xapian-optimize = {
      timerConfig = {
        RandomizedDelaySec = 4 * 3600;
      };
    };

    environment.etc = {
      # When incoming email was marked as spam by rspamd, expose that
      # information as an IMAP label to the user
      "dovecot/sieve_before.d/tag-junk.sieve".text = ''
        require ["imap4flags"];

        if header :contains "X-Spam-Flag" "YES" {
          addflag ["\\Junk"];
        }
      '';

      # When an user manually changes the IMAP label that we use to
      # encode spam-status, pass that information to rspamd to inform
      # later classification decisions
      # Email marked as "not spam" while in folder named "Trash" or
      # "Junk" does not count for obvious reasons
      "dovecot/sieve_flag.d/learn-junk.sieve".text = ''
        require ["vnd.dovecot.pipe", "copy", "imapsieve", "environment", "variables", "imap4flags"];

        if environment :matches "imap.user" "*" {
          set "username" "''${1}";
        }

        if environment :contains "imap.changedflags" "\\Junk" {
          if hasflag "\\Junk" {
            pipe :copy "learn_spam.sh" [ "''${username}" ];
          } else {
            if environment :matches "imap.mailbox" "*" {
              set "mailbox" "''${1}";
            }

            if not anyof(string "''${mailbox}" "Trash", string "''${mailbox}" "Junk") {
              pipe :copy "learn_ham.sh" [ "''${username}" ];
            }
          }
        }
      '';
    };

    # Bespoke Diffie-Hellman parameters
    security.dhparams = {
      params = {
        "postfix-512".bits = 512;
        "postfix-1024".bits = 2048;

        "postfix-smtps-512".bits = 512;
        "postfix-smtps-1024".bits = 2048;
      };
    };

    # TLS certificates for our various domains
    security.acme.rfc2136Domains = {
      "srv01.uniworx.de" = {
        restartUnits = [ "postfix.service" "dovecot2.service" ];
      };
    } // listToAttrs (concatMap (domain: [
      (nameValuePair domain { restartUnits = ["postfix.service" "dovecot2.service"]; })
      (nameValuePair "mailin.${domain}" { restartUnits = ["postfix.service"]; })
      (nameValuePair "mailsub.${domain}" { restartUnits = ["postfix.service"]; })
      (nameValuePair "imap.${domain}" { restartUnits = ["dovecot2.service"]; })
      (nameValuePair "mta-sts.${domain}" { restartUnits = ["nginx.service"]; })
    ]) emailDomains);

    # Pass secrets to postfix
    systemd.services.postfix = {
      serviceConfig.LoadCredential = [
        "srv01.uniworx.de.key.pem:${config.security.acme.certs."srv01.uniworx.de".directory}/key.pem"
        "srv01.uniworx.de.pem:${config.security.acme.certs."srv01.uniworx.de".directory}/fullchain.pem"
      ] ++ concatMap (domain:
        map (subdomain: "${subdomain}.full.pem:${config.security.acme.certs.${subdomain}.directory}/full.pem")
          [domain "mailin.${domain}" "mailsub.${domain}"]
      ) emailDomains;
    };

    systemd.services.dovecot2 = {
      # Before starting dovecot, compile the server-wide sieve scripts
      # for minimally better performance
      preStart = ''
        for f in /etc/dovecot/sieve_flag.d/*.sieve /etc/dovecot/sieve_before.d/*.sieve; do
          ${pkgs.dovecot_pigeonhole}/bin/sievec $f
        done
      '';

      # Pass secrets to dovecot
      serviceConfig = {
        LoadCredential = [
          "srv01.uniworx.de.key.pem:${config.security.acme.certs."srv01.uniworx.de".directory}/key.pem"
          "srv01.uniworx.de.pem:${config.security.acme.certs."srv01.uniworx.de".directory}/fullchain.pem"
        ] ++ concatMap (domain:
          concatMap (subdomain: [
            "${subdomain}.key.pem:${config.security.acme.certs.${subdomain}.directory}/key.pem"
            "${subdomain}.pem:${config.security.acme.certs.${subdomain}.directory}/fullchain.pem"
          ])
            [domain "imap.${domain}"]
        ) emailDomains;
      };
    };

    # MTA-STS require use to host a small file in a well-known
    # location over HTTPS
    # Doing so informes other servers that we support and *demand* TLS
    # transport encryption for incoming email
    services.nginx = {
      virtualHosts = listToAttrs (map (domain: nameValuePair "mta-sts.${domain}" {
        forceSSL = true;
        sslCertificate = "/run/credentials/nginx.service/mta-sts.${domain}.pem";
        sslCertificateKey = "/run/credentials/nginx.service/mta-sts.${domain}.key.pem";
        sslTrustedCertificate = "/run/credentials/nginx.service/mta-sts.${domain}.chain.pem";

        extraConfig = ''
          add_header Strict-Transport-Security "max-age=63072000" always;

          add_header Access-Control-Allow-Origin '*';
          add_header Access-Control-Allow-Methods 'GET, POST, PUT, DELETE, OPTIONS';
          add_header Access-Control-Allow-Headers 'X-Requested-With, Content-Type, Authorization';
          add_header Access-Control-Max-Age 7200;
        '';

        locations."/" = {
          extraConfig = ''
            charset utf-8;
            source_charset utf-8;
          '';
          root = pkgs.runCommand "mta-sts.${domain}" {} ''
            mkdir -p $out/.well-known
            cp ${pkgs.writeText "mta-sts.${domain}.txt" ''
              version: STSv1
              mode: enforce
              max_age: 2419200
              mx: mailin.${domain}
            ''} $out/.well-known/mta-sts.txt
          '';
        };
      }) emailDomains);
    };

    # Pass secrets to nginx
    systemd.services.nginx.serviceConfig.LoadCredential = concatMap (domain: [
      "mta-sts.${domain}.key.pem:${config.security.acme.certs."mta-sts.${domain}".directory}/key.pem"
      "mta-sts.${domain}.pem:${config.security.acme.certs."mta-sts.${domain}".directory}/fullchain.pem"
      "mta-sts.${domain}.chain.pem:${config.security.acme.certs."mta-sts.${domain}".directory}/chain.pem"
    ]) emailDomains;

    # Query other servers' opinions on whether we need to use TLS to
    # talk to them
    services.postfix-mta-sts-resolver = {
      enable = true;
      loglevel = "debug";
    };

    # Bespoke python script for consulting database on whether a given
    # TLS-certificate CN may use a given SMTP envelope from address
    systemd.sockets."postfix-ccert-sender-policy" = {
      requiredBy = ["postfix.service"];
      wants = ["postfix-ccert-sender-policy.service"];
      socketConfig = {
        ListenStream = "/run/postfix-ccert-sender-policy.sock";
      };
    };
    systemd.services."postfix-ccert-sender-policy" = {
      after = [ "postgresql.service" ];
      bindsTo = [ "postgresql.service" ];

      serviceConfig = {
        Type = "notify";

        ExecStart = "${ccert-sender-policy-server}/bin/ccert-sender-policy-server";

        Environment = [
          "PGDATABASE=email"
        ];

        DynamicUser = false;
        User = "postfix-ccert-sender-policy";
        Group = "postfix-ccert-sender-policy";
        ProtectSystem = "strict";
        SystemCallFilter = "@system-service";
        NoNewPrivileges = true;
        ProtectKernelTunables = true;
        ProtectKernelModules = true;
        ProtectKernelLogs = true;
        ProtectControlGroups = true;
        MemoryDenyWriteExecute = true;
        RestrictSUIDSGID = true;
        KeyringMode = "private";
        ProtectClock = true;
        RestrictRealtime = true;
        PrivateDevices = true;
        PrivateTmp = true;
        ProtectHostname = true;
        ReadWritePaths = ["/run/postgresql"];
      };
    };
    users.users."postfix-ccert-sender-policy" = {
      isSystemUser = true;
      group = "postfix-ccert-sender-policy";
    };
    users.groups."postfix-ccert-sender-policy" = {};

    # Bespoke python script for consulting a static whitelist whether
    # a given TLS-certificate CN may use us as an email relay to
    # anywhere
    systemd.sockets."postfix-ccert-relay-policy" = {
      requiredBy = ["postfix.service"];
      wants = ["postfix-ccert-relay-policy.service"];
      socketConfig = {
        ListenStream = "/run/postfix-ccert-relay-policy.sock";
      };
    };
    systemd.services."postfix-ccert-relay-policy" = {
      after = [ "postgresql.service" ];
      bindsTo = [ "postgresql.service" ];

      serviceConfig = {
        Type = "notify";

        ExecStart = let
          cnWhitelist = [ "nextcloud" "gitlab" ];
        in "${ccert-relay-policy-server}/bin/ccert-relay-policy-server ${pkgs.writeText "cn-whitelist.json" (builtins.toJSON cnWhitelist)}";

        DynamicUser = false;
        User = "postfix-ccert-relay-policy";
        Group = "postfix-ccert-relay-policy";
        ProtectSystem = "strict";
        SystemCallFilter = "@system-service";
        NoNewPrivileges = true;
        ProtectKernelTunables = true;
        ProtectKernelModules = true;
        ProtectKernelLogs = true;
        ProtectControlGroups = true;
        MemoryDenyWriteExecute = true;
        RestrictSUIDSGID = true;
        KeyringMode = "private";
        ProtectClock = true;
        RestrictRealtime = true;
        PrivateDevices = true;
        PrivateTmp = true;
        ProtectHostname = true;
      };
    };
    users.users."postfix-ccert-relay-policy" = {
      isSystemUser = true;
      group = "postfix-ccert-relay-policy";
    };
    users.groups."postfix-ccert-relay-policy" = {};

    # Ratelimiting for outgoing email
    services.postfwd = {
      enable = true;
      # 100 emails per hour, per client TLS CN
      # 1000 emails per day, per client TLS CN
      rules = ''
        id=RCPT01; protocol_state=DATA; protocol_state=END-OF-MESSAGE; action=rcpt(ccert_subject/100/3600/set(HIT_RATELIMIT=1,HIT_RATECOUNT=$$ratecount,HIT_RATELIMIT_LIMIT=100,HIT_RATELIMIT_INTERVAL=3600))
        id=RCPT02; protocol_state=DATA; protocol_state=END-OF-MESSAGE; action=rcpt(ccert_subject/1000/86400/set(HIT_RATELIMIT=1,HIT_RATECOUNT=$$ratecount,HIT_RATELIMIT_LIMIT=1000,HIT_RATELIMIT_INTERVAL=86400))

        id=JUMP_REJECT_RL; HIT_RATELIMIT=="1"; action=jump(REJECT_RL)

        id=EOF; action=DUNNO

        id=REJECT_RL; action=450 4.7.1 Exceeding maximum of $$HIT_RATELIMIT_LIMIT recipients per $$HIT_RATELIMIT_INTERVAL seconds [$$HIT_RATECOUNT]
      '';
    };
  };
}
