# Email Services

We configure the server to provide relatively conventional email
infrastructure using, primarily:

  - Postfix (SMTP)
  - Dovecot2 (IMAP)

Broadly, the setup configured here has the following properties:

  - All authentication is done by TLS client certificates, no
    usernames/passwords
	
	Special TLS certificates are available to be allowed to
    arbitrarily use the mailserver as a relay.  
	This is useful for e.g. webservices like NextCloud or GitLab, that
    would like to be able to send emails at their discretion.
  - No support for conventional unix accounts; all mailboxes are
    "virtual"
  - Virtual mailboxes and associated addresses are configured entirely
    in a PostgreSQL database
  - Rspamd provides automatic spam filtering
    
    Classifications made manually by users (setting the IMAP
    `\Junk`-Flag) are fed back into rspamd to improve future automatic
    classification
  - Server-side filtering (Sieve) is available and may be configured
    via the Managesieve protocol by the users themselves
  - Emails submitted by users are checked to specify only __SMTP__
    sender addresses (*ENVELOPE FROM*) that they are associated with
    as recipients in the database
  - Users are ratelimited to only be allowed to send a few hundred
    emails per day each
  - [SRS][], [DKIM][], [MTA-STS][], [DANE][] are all supported

[SRS]: https://en.wikipedia.org/wiki/Sender_Rewriting_Scheme
[DKIM]: https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail
[MTA-STS]: https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol#SMTP_MTA_Strict_Transport_Security
[DANE]: https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities

## CA

The `ca`-folder contains a private/public keypair (`ca.crt`, `ca.key`)
acting as a certificate authority for all services associated with
email.

Use the [CA Utility][ca], which is available in the development shell
and via direnv, to manage/generate certificates (see `ca --help`).

[ca]: https://gitlab.com/gkleen/ca

Certificate common names correspond to `mailbox.mailbox` entries in
PostgreSQL and are used to make authorization decisions.
