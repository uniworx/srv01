{ flakeInputs, flake, lib, pkgs, config, ... }:

with lib;

let
  # Random but static `/etc/machine-id` and hostId; sometimes useful
  # for dhcp and to not have systemd and dbus freak out
  machineId ="b8e5adc211d3cd03a5aac8e763b88210";
in {
  imports = with flakeInputs; [
    # Include nixos modules provided by flake inputs so they may be
    # used in this system configuration
    sops-nix.nixosModules.sops home-manager.nixosModules.home-manager

    ./users/gkleen
    ./users/savau
    ./users/jost
    ./users/mosbach

    ./tls ./adns ./email ./postgresql ./nextcloud ./backups

    ./uniworx-dev ./kubernetes
  ];

  config = {
    # Make current git rev available to running system
    # Use `nixos-version --json` to retrieve it
    # Useful for debugging because flakeyness ensures that output
    # *actually always matches* the git revision from which the
    # currently deployed configuration was built
    system.configurationRevision = mkIf (flake ? rev) flake.rev;

    nixpkgs = {
      # Ensure that version of nixpkgs being used by system is
      # *actually exactly* the same as the nixpkgs input to the flake
      # and not some nixos channel weirdness
      pkgs = flake.legacyPackages.${config.nixpkgs.system}.override {
        inherit (config.nixpkgs) config;
        localSystem = config.nixpkgs.system;
      };

      system = "x86_64-linux";
    };

    nix = {
      settings = {
        # Sandbox nix builds performed on this machine
        sandbox = true;
        # Anyone may build
        allowed-users = [ "*" ];
        # We only trust root and sudoers to e.g. import unsigned
        # archives into nix stores
        trusted-users = [ "root" "@wheel" ];
      };

      # Use copy of nixpkgs flake input as `<nixpkgs>` for everything
      # (e.g. nix-shell)
      nixPath = [
        "nixpkgs=/run/nixpkgs"
      ];

      extraOptions = ''
        experimental-features = nix-command flakes
      '';

      # Provide copies of all flake inputs to nix
      # Rename self to nixos in analogy to nixos channel
      registry =
        { machines = {
            exact = false;
            to = {
              type = "git";
              url = "https://gitlab.com/uniworx/srv01.git";
              submodules = true;
            };
          };
        } //
        (let override = { self = "nixos"; };
         in lib.mapAttrs' (inpName: inpFlake: lib.nameValuePair
           (override.${inpName} or inpName)
           { flake = inpFlake; } ) flakeInputs);
    };

    systemd.tmpfiles.rules = [
      # Actually provide `/run/nixpkgs` copy of nixpkgs flake input
      # used above
      "L+ /run/nixpkgs - - - - ${flakeInputs.nixpkgs.outPath}"
    ];

    users.mutableUsers = false;

    home-manager = {
      # Pass system configuration `pkgs` argument to home-manager
      # configurations instead of using user-local nixpkgs'
      useGlobalPkgs = true;

      # Install packages for users by overriding their $PATH during
      # home-manager activation locally instead of using
      # `config.users.users.<name>.packages` system configuration
      # option
      useUserPackages = false;
    };

    # sops is our secret management framework
    # Secrets that should be stable across reinstalls are stored
    # encrypted in this flake (i.e. in git) and decrypted on system
    # activation (e.g. boot) using the secret key of an asymmetric
    # keypair created for this purpose
    # Secrets may be encrypted for other keypairs additionally, this
    # allows convenient manipulation by sysadmins
    sops = {
      # Disable all gnugp support, we want to use age instead
      gnupg = {
        home = null;
        sshKeyPaths = [];
      };

      # Decrypt secrets using age, using key stored persistently in
      # `/var/lib/sops-nix` -- don't generate a key from the systems
      # ssh keys (SSH host keys will be secrets decrypted via age
      # instead)
      age = {
        keyFile = "/var/lib/sops-nix/key.txt";
        generateKey = false;
        sshKeyPaths = [];
      };
    };

    environment.systemPackages = with pkgs; [
      # Because we're flakey
      git
      # Because we want to be able to e.g. generate new keys from cli
      rage
    ];

    # Store approximate nixpkgs version under which system was first
    # installed
    # Future versions attempt to stay compatible to existing state
    # (and/or migrate it properly) when updates are performed IFF the
    # version stored in configuration is accurate and remains stable
    system.stateVersion = "22.11";

    # Provide `machineId` to running system
    networking.hostId = substring 0 8 machineId;
    environment.etc."machine-id".text = machineId;

    boot = {
      # KVM virtualisation at netcup is legacy bios only, no UEFI
      loader = {
        efi = {
          canTouchEfiVariables = true;
          efiSysMountPoint = "/boot";
        };
        grub = {
          enable = true;
          efiSupport = true;
          # Strategically chosen for ~50MiB worth of configurations in `/boot`
          configurationLimit = 7;
          version = 2;
          device = "nodev";
        };
      };

      tmpOnTmpfs = true;

      # Include support for ZFS
      initrd.supportedFilesystems = ["zfs"];
      supportedFilesystems = ["zfs"];

      # We prefer not to configure ZFS filesystems statically in
      # fileSystems and instead have zfs mount all available
      # filesystems to their proper mountpoints as early during boot
      # as possible
      # Some exceptions to this exist and are configured below --
      # remember to include all ZFS filesystems needed for the
      # activation script in static configuration!
      postBootCommands = ''
        ${config.boot.zfs.package}/bin/zfs mount -a
      '';

      # `/dev/vda*` does not usually show up in `/dev/disk/by-id`;
      # search everywhere instead
      zfs.devNodes = "/dev";

      # Retrieve precision time information from hypervisor; results
      # in better clock accuracy even when disciplined by NTP
      kernelModules = ["ptp_kvm"];

      # QEMU guest
      initrd.kernelModules = [ "virtio_net" "virtio_pci" "virtio_mmio" "virtio_blk" "virtio_scsi" "9p" "9pnet_virtio" "virtio_balloon" "virtio_console" "virtio_rng" ];
    };
    services.qemuGuest.enable = true;

    networking = {
      hostName = "srv01";
      domain = "uniworx.de";
      search = [ "uniworx.de" ];

      enableIPv6 = true;

      # Static network configuration only
      dhcpcd.enable = false;
      useDHCP = false;

      # Networkd is a good idea for complicated networking setups with
      # e.g. VPNs
      useNetworkd = true;

      # Basic static networking configuration; complicated stuff is
      # done directly via networkd
      defaultGateway = { address = "89.58.4.1"; };
      defaultGateway6 = { address = "fe80::1"; };
      interfaces."ens3" = {
        ipv4.addresses = [
          { address = "89.58.4.219"; prefixLength = 22; }
        ];
        ipv6.addresses = [
          { address = "2a03:4000:5e:e55::"; prefixLength = 64; }
        ];
      };

      # iptables sucks, use nftables directly
      firewall.enable = false;
      nftables = {
        enable = true;
        rulesetFile = ./ruleset.nft;
      };

      # No systemd-resolved; use `/etc/resolv.conf` instead, which
      # shall be managed by resolvconf
      resolvconf = {
        enable = true;
        # We host our own recursive DNS resolver (unbound); just
        # configure that and nothing else
        useLocalResolver = true;
      };
    };

    boot.kernel.sysctl = {
      "net.ipv6.conf.all.forwarding" = true;
      "net.ipv6.conf.default.forwarding" = true;
      "net.ipv4.conf.all.forwarding" = true;
      "net.ipv4.conf.default.forwarding" = true;
    };

    # No systemd-resolved; it's just not reliable
    services.resolved.enable = false;

    # We host our own recursive DNS resolver (unbound) instead of
    # using e.g. 8.8.8.8 or 1.1.1.1 to resolve DNS for us
    # Because why not?
    services.unbound = {
      enable = true;
      # Don't have unbound module try to configure `/etc/resolv.conf`
      # We do that ourselves above
      resolveLocalQueries = false;
      stateDir = "/var/lib/unbound";
      # Enable "remote control", i.e. allow use of cli tool to
      # configure running instance
      localControlSocketPath = "/run/unbound/unbound.ctl";
      # Don't let unbound manage the `root.hints` file (root of trust
      # for dnssec), we just take that from nixpkgs
      enableRootTrustAnchor = false;

      settings = {
        server = {
          # Local queries only
          interface = ["lo"];
          access-control = ["127.0.0.0/8 allow" "::1/128 allow"];
          # The future is now!
          prefer-ip6 = true;

          # Extract trust anchor from nixpkgs
          root-hints = "${pkgs.dns-root-data}/root.hints";
          trust-anchor-file = "${pkgs.dns-root-data}/root.key";
          trust-anchor-signaling = false;

          # Performance tuning
          num-threads = 12;
          so-reuseport = true;
          msg-cache-slabs = 16;
          rrset-cache-slabs = 16;
          infra-cache-slabs = 16;
          key-cache-slabs = 16;

          rrset-cache-size = "100m";
          msg-cache-size = "50m";
          outgoing-range = 8192;
          num-queries-per-thread = 4096;

          so-rcvbuf = "4m";
          so-sndbuf = "4m";

          # Keep items in cache by proactively fetching them again
          # before they expire
          prefetch = true;
          prefetch-key = true;

          # We only serve localhost, don't optimize for network bandwidth
          minimal-responses = false;

          # Heuristics to make spoofing attempts less likely to
          # succeed in the absence of dnssec
          rrset-roundrobin = true;
          use-caps-for-id = true;
        };
      };
    };

    # systemd-timesyncd sucks
    services.timesyncd.enable = false;
    # NTP client to keep our clock synchronized
    services.chrony = {
      enable = true;
      # Module config is not powerful enough to express our desires
      servers = [];
      # Use authenticated european time sources, discipline clock
      # using realtime clock provided by hypervisor for better
      # stability, communicate current leapsecond table to kernel so
      # the kernel can in turn provide an accurate TAI clock,
      # aggressively adjust clock to be correct at all times
      extraConfig = ''
        pool time.cloudflare.com iburst nts
        pool nts.ntp.se iburst nts
        server nts.sth1.ntp.se iburst nts
        server nts.sth2.ntp.se iburst nts
        server ptbtime1.ptb.de iburst nts
        server ptbtime2.ptb.de iburst nts
        server ptbtime3.ptb.de iburst nts

        refclock PHC /dev/ptp_kvm poll 2 dpoll -2 offset 0 stratum 3

        leapsectz right/UTC

        makestep 0.1 3

        cmdport 0
      '';
    };

    services.openssh = {
      enable = true;
      # Definitely no password authentication
      passwordAuthentication = false;
      kbdInteractiveAuthentication = false;

      # Configure host keys manually
      hostKeys = mkForce [];

      logLevel = "VERBOSE";

      # Harden crypto
      ciphers = [ "chacha20-poly1305@openssh.com" "aes256-gcm@openssh.com" "aes256-ctr" ];
      macs = [ "hmac-sha2-256-etm@openssh.com" "hmac-sha2-256" "hmac-sha2-512-etm@openssh.com" "hmac-sha2-512" ];
      kexAlgorithms = [ "curve25519-sha256@libssh.org" "diffie-hellman-group-exchange-sha256" ];

      # Further harden crypto, provide host keys with matching
      # certificates signed by ssh-ca, allow users to log in using
      # certificates signed by ssh-ca
      extraConfig = ''
        HostKeyAlgorithms sk-ssh-ed25519-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,ssh-ed25519,rsa-sha2-256,rsa-sha2-512
        CASignatureAlgorithms sk-ssh-ed25519@openssh.com,ssh-ed25519,rsa-sha2-256,rsa-sha2-512
        PubkeyAcceptedAlgorithms ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-ed25519,ssh-rsa

        HostKey ${config.sops.secrets.ssh_host_rsa_key.path}
        HostCertificate /etc/ssh/ssh_host_rsa_key-cert.pub
        HostKey ${config.sops.secrets.ssh_host_ed25519_key.path}
        HostCertificate /etc/ssh/ssh_host_ed25519_key-cert.pub

        TrustedUserCAKeys ${./ssh/ca.pub}
      '';
    };

    programs.ssh = {
      knownHosts = {
        # Trust host keys signed by ssh-ca
        "*.uniworx.de" = {
          certAuthority = true;
          publicKeyFile = ./ssh/ca.pub;
        };
      };

      # Harden
      ciphers = [ "chacha20-poly1305@openssh.com" "aes256-gcm@openssh.com" "aes256-ctr" ];
      hostKeyAlgorithms = [ "sk-ssh-ed25519-cert-v01@openssh.com" "ssh-ed25519-cert-v01@openssh.com" "rsa-sha2-256-cert-v01@openssh.com" "rsa-sha2-512-cert-v01@openssh.com" "sk-ssh-ed25519@openssh.com" "ssh-ed25519" "rsa-sha2-256" "rsa-sha2-512" ];
      kexAlgorithms = [ "curve25519-sha256@libssh.org" "diffie-hellman-group-exchange-sha256" ];
      macs = [ "umac-128-etm@openssh.com" "hmac-sha2-256-etm@openssh.com" "hmac-sha2-512-etm@openssh.com" "umac-128@openssh.com" "hmac-sha2-256" "hmac-sha2-512" "umac-64-etm@openssh.com" "umac-64@openssh.com"];
      pubkeyAcceptedKeyTypes = [  "ssh-ed25519-cert-v01@openssh.com" "sk-ssh-ed25519-cert-v01@openssh.com" "rsa-sha2-512-cert-v01@openssh.com" "rsa-sha2-256-cert-v01@openssh.com" "ssh-ed25519" "ssh-rsa" ];
      extraConfig = ''
        Host *
          CASignatureAlgorithms sk-ssh-ed25519@openssh.com,ssh-ed25519,rsa-sha2-256,rsa-sha2-512
          PasswordAuthentication no
          KbdInteractiveAuthentication no
      '';
    };
    # ssh-agent should be configured via home-manager per user, if at
    # all
    systemd.user.services."ssh-agent".enable = mkForce false;

    sops.secrets = {
      ssh_host_rsa_key = {
        format = "binary";
        path = "/etc/ssh/ssh_host_rsa_key";
        sopsFile = ./ssh/host_rsa_key;
      };
      ssh_host_ed25519_key = {
        format = "binary";
        path = "/etc/ssh/ssh_host_ed25519_key";
        sopsFile = ./ssh/host_ed25519_key;
      };
    };
    environment.etc = {
      "ssh/ssh_host_rsa_key.pub".source = ./ssh/host_rsa_key.pub;
      "ssh/ssh_host_ed25519_key.pub".source = ./ssh/host_ed25519_key.pub;
      "ssh/ssh_host_rsa_key-cert.pub".source = ./ssh/host_rsa_key-cert.pub;
      "ssh/ssh_host_ed25519_key-cert.pub".source = ./ssh/host_ed25519_key-cert.pub;

      "ssh/krl.bin".source = ./ssh/krl.bin;
    };

    # `sudo` does not get to maintain state; have it not lecture
    # sudoers every reboot
    security.sudo.extraConfig = ''
      Defaults lecture = never
    '';

    # Root on tmpfs
    fileSystems."/" = {
      fsType = "tmpfs";
      options = [ "mode=0755" ];
    };

    fileSystems."/boot" = {
      device = "/dev/disk/by-label/srv01-boot";
      fsType = "vfat";
    };

    swapDevices = [
      { device = "/dev/disk/by-label/srv01-swap";
      }
    ];

    # Required to be configured statically for proper boot
    fileSystems."/nix" = {
      device = "srv01/local/nix";
      fsType = "zfs";
    };

    # Required to be configuried statically because needed in activation script (sops secrets)
    fileSystems."/var/lib/sops-nix" = {
      device = "srv01/local/var-lib-sops--nix";
      fsType = "zfs";
      # Needed during early boot because age key for sops secrets is
      # stored there
      neededForBoot = true;
    };

    services.zfs = {
      # Real filesystems (e.g. ZFS) require periodic scrubbing
      #
      # During a scrub all data is read back from storage and checked
      # against checksums stored beside it.
      # Some error detection and correction also takes place.
      # Periodic scrubbing ensures that *all* data written to the
      # filesystem is readable *exactly as it was saved* at *all*
      # times.
      # Less "real" filesystems like ext4 or ntfs do not provide this
      # guarantee.
      autoScrub.enable = true;

      # Report all zero blocks to underlying software/hardware stack
      # periodically
      trim.enable = true;
    };

    # Generating our own bespoke Diffie-Hellman parameters for our
    # various services prevents well-funded attackers (nation states)
    # from using tables they pre-computed for the default parameters
    # to attack the security of our encrypted connections.
    security.dhparams = {
      enable = true;
      defaultBitSize = 4096;
      params = {
        nginx = {};
      };
      stateful = true;
    };

    # Webserver
    #
    # We expect modules in other places to add virtualHosts below this
    # config for our various services
    services.nginx = {
      enable = true;
      recommendedGzipSettings = true;
      recommendedProxySettings = true;
      recommendedTlsSettings = true;

      # Actually use the bespoke Diffie-Hellman parameters discussed
      # above
      sslDhparam = config.security.dhparams.params.nginx.path;

      # We prefer safe curves over ones tainted by involvement of the
      # NSA in their development/specification
      #
      # Write all logs to systemd journal
      commonHttpConfig = ''
        ssl_ecdh_curve X448:X25519:prime256v1:secp521r1:secp384r1;

        log_format main
                '$remote_addr "$remote_user" '
                '"$host" "$request" $status $bytes_sent '
                '"$http_referer" "$http_user_agent" '
                '$gzip_ratio';

        access_log syslog:server=unix:/dev/log main;
        error_log syslog:server=unix:/dev/log info;
      '';
    };

    # NixOS by default configures a configuration test before nginx
    # gets started/reloaded.
    #
    # We want to use the systemd credentials system to pass secrets to
    # nginx. These secrets are then not available during
    # `ExecStartPre` and `ExecReload` (currently!). Nginx also checks
    # availability of secrets during the configuration test.
    #
    # We therefore disable the configuration test entirely
    systemd.services.nginx = {
      preStart = lib.mkForce config.services.nginx.preStart;
      serviceConfig = {
        ExecReload = lib.mkForce "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
      };
    };
  };
}
