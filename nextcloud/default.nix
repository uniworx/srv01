{ config, lib, ... }:

with lib;

let
  containerConfig = config.containers.nextcloud.config;
in {
  config = {
    # Isolate NextCloud into a systemd-nspawn container
    containers.nextcloud = {
      # Container maintains no state, root on ramfs
      ephemeral = true;

      # Set up a virtual ethernet tunnel for communication between
      # host system and container with the following IPs
      privateNetwork = true;
      hostAddress = "10.248.148.0";
      hostAddress6 = "fdf8:9491:e736::";
      localAddress = "10.248.148.1";
      localAddress6 = "fdf8:9491:e736::1";

      bindMounts = {
        # Container may access `/var/lib/nextcloud` to store files and
        # other state
        "${containerConfig.services.nextcloud.datadir}" = {
          hostPath = "/var/lib/nextcloud";
          isReadOnly = false;
        };
        # Container may also access system-wide PostgreSQL
        "/run/postgresql" = {};
      };

      # Pass secrets to systemd-nspawn for it to make available in
      # container
      extraFlags = [
        "--load-credential=adminpass:${config.sops.secrets."nextcloud-adminpass".path}"
        "--load-credential=email.key:${config.sops.secrets."nextcloud-email".path}"
      ];

      # NixOS allows us to completely specify the container by
      # specifying a subordinate NixOS configuration
      config = let
        hostConfig = config;
      in { pkgs, ... }: {
        config = {
          # NextCloud within container
          services.nextcloud = {
            enable = true;

            # Newer version than default on NixOS 22.11
            package = pkgs.nextcloud25;

            # Disable appstore, provide some apps via NixOS
            appstoreEnable = false;
            extraAppsEnable = true;
            extraApps = {
              twofactor_totp = pkgs.fetchNextcloudApp {
                url = "https://github.com/nextcloud-releases/twofactor_totp/releases/download/v6.4.1/twofactor_totp-v6.4.1.tar.gz";
                sha256 = "sha256-zAPNugbvngXcpgWJLD78YAg4G1QtGaphx1bhhg7mLKE=";
              };
            };

            # NextCloud is accessed under https://cloud.uniworx.de
            https = true;
            hostName = "cloud.uniworx.de";

            maxUploadSize = "2G";

            webfinger = true; # ???

            config = {
              # We get all connections proxied via nginx running on
              # container host
              trustedProxies = [ hostConfig.containers.nextcloud.hostAddress6 ];

              # Admin password is passed as a systemd secret
              adminuser = "admin";
              adminpassFile = "/run/credentials/nextcloud-setup.service/adminpass";

              # Have NextCloud use PostgreSQL
              dbtype = "pgsql";
              dbhost = "/run/postgresql";

              defaultPhoneRegion = "DE";
            };
            extraOptions = {
              # NextCloud email configuration for using client
              # certificate is highly magical
              mail_smtphost = "mailin.uniworx.de";
              mail_smtpport = 25;
              mail_smtpsecure = "tls";
              mail_smtpauth = false;
              mail_smtpstreamoptions.ssl = {
                SNI_enabled = true;
                local_cert = ../email/ca/nextcloud.crt;
                local_pk = "/run/nextcloud/email.key";
              };
              mail_smtpdebug = true;

              "overwrite.cli.url" = "https://cloud.uniworx.de";
            };
          };

          # Basic network setup is analogous to container host
          services.resolved.enable = false;
          networking = {
            firewall.enable = false;
            nftables = {
              enable = true;
              rulesetFile = ./container-ruleset.nft;
            };
          };
          environment.etc."resolv.conf".text = ''
            nameserver 2620:fe::fe
            nameserver 2620:fe::9
            nameserver 9.9.9.9
            nameserver 149.112.112.112
          '';

          # Copy credentials from ones passed to systemd-nspawn on
          # container creation by container host
          #
          # Use `/run/nextcloud` to make `email.key` available both to
          # nextcloud-setup.service and running nextcloud instances.
          # Since both of those use the same configuration (and have
          # no support for environment variable interpolation), we
          # can't otherwise configure a static path like
          # `/run/credentials/nextcloud-setup.service` since that path
          # would contain the systemd unit name.
          systemd.services."nextcloud-setup" = {
            after = [ "run-nextcloud.mount" ];
            bindsTo = [ "run-nextcloud.mount" ];

            script = mkBefore ''
              cat </run/credentials/nextcloud-setup.service/email.key >/run/nextcloud/email.key
            '';

            serviceConfig = {
              ExecStartPre = "+${pkgs.writeShellScript "nextcloud-setup-pre" ''
                chown nextcloud:nextcloud /run/nextcloud
                chmod 0750 /run/nextcloud
                install -m 0640 -o nextcloud -g nextcloud /dev/null /run/nextcloud/email.key
              ''}";

              LoadCredential = [
                "email.key:/run/credentials/@system/email.key"
                "adminpass:/run/credentials/@system/adminpass"
              ];
            };
          };
          systemd.services."nextcloud-cron" = {
            after = [ "run-nextcloud.mount" ];
            bindsTo = [ "run-nextcloud.mount" ];
          };
          systemd.services."phpfpm-nextcloud" = {
            after = [ "run-nextcloud.mount" ];
            bindsTo = [ "run-nextcloud.mount" ];
          };
          systemd.mounts = [
            { what = "ramfs";
              where = "/run/nextcloud";
              type = "ramfs";
              options = "rw,nosuid,nodev,noexec,relatime";
              mountConfig = {
                DirectoryConfig = "0000";
              };
            }
          ];

          # UID and GID for nextcloud and postgresql need to be the
          # same on container host and in container
          users.users.nextcloud.uid = 900;
          users.groups.nextcloud.gid = 900;

          users.users.postgres = {
            uid = hostConfig.ids.uids.postgres;
            group = "postgres";
            home = "/var/empty";
            useDefaultShell = true;
          };
          users.groups.postgres.gid = hostConfig.ids.gids.postgres;

          # Have nginx log to systemd journal in container
          services.nginx = {
            commonHttpConfig = ''
              log_format main
                      '$remote_addr "$remote_user" '
                      '"$host" "$request" $status $bytes_sent '
                      '"$http_referer" "$http_user_agent" '
                      '$gzip_ratio';

              access_log syslog:server=unix:/dev/log main;
              error_log syslog:server=unix:/dev/log info;
            '';
          };

          system.stateVersion = hostConfig.system.stateVersion;
        };
      };
    };

    # Restart container if configuration is changed; useful for
    # debugging
    systemd.services."container@nextcloud" = {
      after = ["postgresql.service"];
      bindsTo = ["postgresql.service"];

      restartIfChanged = mkForce true;
    };

    # UID and GID for nextcloud needs to be the same on container host
    # and in container
    users.users.nextcloud = {
      home = "/var/empty";
      group = "nextcloud";
      isSystemUser = true;
      uid = containerConfig.users.users.nextcloud.uid;
    };
    users.groups.nextcloud = {
      gid = containerConfig.users.groups.nextcloud.gid;
    };

    # Secrets to pass through to systemd-nspawn container
    sops.secrets = {
      "nextcloud-adminpass" = {
        format = "binary";
        sopsFile = ./adminpass.txt;
      };
      "nextcloud-email" = {
        format = "binary";
        sopsFile = ../email/ca/nextcloud.key;
      };
    };

    # Custom hostname for container for nginx upstream (reverse proxy)
    networking.extraHosts = ''
      ${config.containers.nextcloud.localAddress6} nextcloud.containers
    '';

    # Pass TLS secrets for cloud.math.lmu.de to nginx on container
    # host
    systemd.services.nginx = {
      wants = [ "container@nextcloud.service" ];
      after = [ "container@nextcloud.service" ];

      serviceConfig.LoadCredential = [
        "cloud.uniworx.de.key.pem:${config.security.acme.certs."cloud.uniworx.de".directory}/key.pem"
        "cloud.uniworx.de.pem:${config.security.acme.certs."cloud.uniworx.de".directory}/fullchain.pem"
        "cloud.uniworx.de.chain.pem:${config.security.acme.certs."cloud.uniworx.de".directory}/chain.pem"
      ];
    };

    # Provision TLS certificate for cloud.uniworx.de
    security.acme.rfc2136Domains = {
      "cloud.uniworx.de" = {
        restartUnits = [ "nginx.service" ];
      };
    };

    services.nginx = {
      upstreams.nextcloud = {
        servers = {
          "nextcloud.containers" = {};
        };
      };

      # Reverse proxy cloud.uniworx.de to container
      virtualHosts."cloud.uniworx.de" = {
        forceSSL = true;
        sslCertificate = "/run/credentials/nginx.service/cloud.uniworx.de.pem";
        sslCertificateKey = "/run/credentials/nginx.service/cloud.uniworx.de.key.pem";

        locations."/" = {
          proxyPass = "http://nextcloud";
        };
      };
    };
  };
}
