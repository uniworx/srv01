# NextCloud

This server runs a private NextCloud instance.

The installation is largely default, except that the NextCloud service
runs in a [systemd-nspawn container][systemd-nspawn].  
This is because the default NixOS-installation insists on modifying
the system-wide nginx configuration to provide support for executing
arbitrary PHP-scripts.  
By separating the installation into a container we avoid "infecting"
the entire system with PHP.

[systemd-nspawn]: https://wiki.archlinux.org/title/systemd-nspawn

NextCloud accesses the system-wide PostgreSQL database server via unix
sockets passed through to the container via bind-mount.

NextCloud may send emails by using the system-wide postfix SMTP server
as a relay because it has been provided with a TLS client certificate
registered with postfix as being allowed to relay arbitrary mail.
