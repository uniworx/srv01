{ pkgs, config, flakeInputs, lib, ... }:

with lib;

let
  # Handy alias for specification of borg repository host
  #
  # Use specific SSH Key for access to repository host
  #
  # Verify repository host by SSH CA so no manual acceptance of
  # fingerprints is ever needed
  sshConfig = ''
    Include /etc/ssh/ssh_config

    ControlMaster auto
    ControlPath /var/lib/borg/.borgssh-master-%r@%n:%p
    ControlPersist yes

    Host borg.vidhar
      HostName vidhar.yggdrasil.li
      User borg
      IdentityFile ${config.sops.secrets."append.borg.vidhar".path}
      IdentitiesOnly yes
      UpdateHostKeys no
      UserKnownHostsFile ${pkgs.writeText "ssh_known_hosts" ''
        @cert-authority *.yggdrasil.li ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC1t7HamptQ49VXtSZyRsaOuja5In1N0U9Ybdiu6ztzi ca.yggdrasil
      ''}

      BatchMode yes
      ServerAliveInterval 10
      ServerAliveCountMax 30

      IPQoS cs1
  '';
in {
  imports = with flakeInputs; [
    backup-utils.nixosModules.default k8s-gitlab-borg.nixosModules.default
  ];

  config = {
    services.zfssnap = {
      # Take frequent ZFS snapshots of all volumes configured with
      # property `li.yggdrasil:auto-snapshot=true`
      enable = true;

      # Retention schedule of ZFS snapshots
      #
      # Snapshots that exist beyond the configured retention are
      # automatically destroyed
      config.keep = {
        within = "15m";
        "5m" = "48";
        "15m" = "32";
        hourly = "48";
        "4h" = "24";
        "12h" = "12";
        daily = "14";
        halfweekly = "0";
        weekly = "0";
        monthly = "0";
      };
    };

    services.borgsnap = {
      # Convert ZFS snapshots to borg archives stored offsite
      enable = true;

      # Which snapshots to create borg archives for
      #
      # Note: this is currently not a configuration of retention
      # times; borg archives are currently kept forever
      extraConfig = mkForce {
        daily = "31";
        monthly = "6";
      };

      # Where to store borg archives
      target = "borg.vidhar:.";
      archive-prefix = "de.uniworx.srv01.";

      inherit sshConfig;
    };

    services.k8s-gitlab-borg = {
      enable = true;

      target = "borg.vidhar:.";
      archive-prefix = "de.uniworx.gitlab";
      config = {
        keep.within = "12h";
        keep."4h".count = 0;
        keep.daily.count = 0;

        copy.daily.count = 31;
        copy.monthly.count = 6;
      };
      execInterval = "*-*-* 00/4:00";

      inherit sshConfig;
    };

    sops.secrets."append.borg.vidhar" = {
      format = "binary";
      sopsFile = ./append.vidhar;
      owner = "borg";
      group = "borg";
      mode = "0400";
    };

    # User for backup jobs to run as
    users.users.borg = {
      useDefaultShell = true;
      isSystemUser = true;
      group = "borg";
    };
    users.groups.borg = {};
  };
}
