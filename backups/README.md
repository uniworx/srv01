# Backups

This server uses [backup-utils][] to take frequent snapshots of most
ZFS volumes and then store some of these snapshots in the form of
[borg][]-archives on a remote host.

[backup-utils]: https://gitlab.com/gkleen/backup-utils
[borg]: https://www.borgbackup.org/

Currently viðar (infrastructure belongs to @gkleen) serves as a borg
repository host.

__TODO__: There is currently no provision for automatically deleting
old borg archives.

Old ZFS snapshots are automatically destroyed in accordance with
configured retention rules.
