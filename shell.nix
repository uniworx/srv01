inputs@{ self, ca-util, ... }:

let
  pkgs = self.legacyPackages."x86_64-linux";
in pkgs.mkShell {
  name = "nixos";
  nativeBuildInputs = with pkgs; [
    # Provides `nom`, for better output of `nix build`
    nix-output-monitor

    # Encryption of secrets
    sops rage

    # Minimal build system
    gup

    # Provides `ca` cli utility for managing/generating TLS certificates/privkeys/...
    ca-util.packages."x86_64-linux".ca

    util-linux # for `uuidgen`
    knot-dns # for `keymgr`

    # Interpreter for various shell scripts
    zsh
  ];
}
