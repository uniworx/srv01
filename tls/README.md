# TLS Certificates

We provision TLS certificates by solving [ACME DNS-01 challenges][]
for Let's Encrypt.

[ACME DNS-01 challenges]: https://letsencrypt.org/docs/challenge-types/#dns-01-challenge

At system boot a service starts that automatically checks if the TLS
certificate is expired or missing.

If it is, we contact Let's Encrypt and receive a temporary token.  
We then communicate with our ADNS (knot running locally) to insert
that token as a new DNS record into our zone.  
For that communication to take place knot and the service doing the
provisioning on our end need to share a secret key to secure their
it.

Generate a new such key for domain `example.org` as follows:
```bash
gup tsig_keys/example.org
```

This first generates a new tsig key in `../adns/keys` and then copies
the contents into `tsig_keys`.

## Configuration conventions

`default.nix` does not contain any explicit setup for any domains.  
Instead it defines a new nixos option `security.acme.rfc2136Domains`.

Whenever a certificate is required (e.g. when setting up a webserver)
we just locally configure the domain we need, e.g.:
```nix
nginx = {
  enable = true;

  ... # Assume config mentions `uniworx.de`
};

security.acme.rfc2136Domains = {
  "uniworx.de" = {};
};
```
