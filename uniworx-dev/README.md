# Environment as a Uni2work development machine

All that is needed to use this server for developing Uni2work is to
provide our project-wide fontawesome token to all users.

SSH access is configured elsewhere (`../ssh`).
