{ config, ... }:
{
  config = {
    sops.secrets = {
      fontawesome_token = {
        sopsFile = ./fontawesome-token;
        format = "binary";
        mode = "0440";
        group = "users";
        path = "/etc/fontawesome-token";
      };

      nix-netrc = {
        sopsFile = ./netrc;
        format = "binary";
        path = "/etc/nix/netrc";
      };
    };

    nix.settings = {
      netrc-file = config.sops.secrets.nix-netrc.path;
    };

    systemd.slices."user".sliceConfig = {
      CPUAccounting = true;
      CPUQuota = "1000%";
      MemoryAccounting = true;
      MemoryHigh = "40G";
    };

    systemd.services."nix-daemon".serviceConfig = {
      CPUAccounting = true;
      CPUQuota = "200%";
      MemoryAccounting = true;
      MemoryHigh = "20G";
    };
  };
}
