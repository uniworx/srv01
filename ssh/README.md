# SSH PKI

This directory contains a SSH certificate authority, and ssh host keys and associated certificates.

See [The OpenSSH Cookbook](https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Certificate-based_Authentication) for details.

Signing of host keys has been automated for convenience, e.g.:
```bash
gup -u *-cert.pub
```

Sensible expiration times are determined automatically.  
Certificates are given random identities and [TAI64
labels](https://cr.yp.to/libtai/tai64.html) as serial numbers.
The latter allows revoking certificates by signature time which is far
more convenient than an arbitrary monotonic counter.  
Some care is automatically taken not to reuse serial numbers.

Analogously user certificates may be created with the provided script, e.g.:
```bash
./sign-user-key.py --principal gkleen -- ~/.ssh/gkleen@uniworx.de.pub
```
creates `~/.ssh/gkleen@uniworx.de-cert.pub` with a sensible expiration
time and the above scheme wrt. serial number and identity.
