#! /usr/bin/env nix-shell
#! nix-shell -i python -p python39

import os, sys

import argparse
import subprocess

from time import sleep

from pathlib import Path

from tempfile import TemporaryDirectory


def main():
    def str_no_comma(inp_str):
        if ',' in inp_str:
            raise argparse.ArgumentTypeError('Argument may not contain commas')

        return inp_str

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--principal', '-p', metavar='PRINCIPAL', nargs='+', type=str_no_comma, dest='principals', required=True)
    parser.add_argument('--output', '-o', type=Path, metavar='SSH_CERTIFICATE_FILE')
    parser.add_argument('key_file', type=Path, metavar='SSH_PUBKEY_FILE')
    args = parser.parse_args()

    if not args.output:
        args.output = args.key_file.with_stem(f'{args.key_file.stem}-cert')

    subprocess.run(['gup', '-u', 'expiration'], check=True, env=os.environ | { 'TZ': 'UTC' })
    expiration = None
    with open('expiration', 'r') as fh:
        expiration = fh.read().strip()
    if not expiration:
        raise RuntimeError('File ‘expiration’ appears to be empty')

    uuid_res = subprocess.run(['uuidgen'], check=True, stdout=subprocess.PIPE)
    uuid = uuid_res.stdout.decode('utf-8').strip()

    serial_res = subprocess.run(['tai64dec', '--no-ns'], check=True, stdout=subprocess.PIPE)
    serial = serial_res.stdout.decode('utf-8').strip()

    with TemporaryDirectory(prefix="ssh-") as tmpdir:
        ssh_auth_sock = Path(tmpdir) / "agent"
        with subprocess.Popen(['ssh-agent', '-a', ssh_auth_sock, "-D"], stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL) as agent_proc:
            with subprocess.Popen(['ssh-add', '-'], env=os.environ | {'SSH_AUTH_SOCK': ssh_auth_sock}, stdin=subprocess.PIPE) as add_proc:
                with subprocess.Popen(['sops', '-d', 'ca'], stdout=subprocess.PIPE) as sops_proc:
                    add_proc.stdin.write(sops_proc.stdout.read())
                add_proc.stdin.close()

            subprocess.run(['ssh-keygen', '-Us', 'ca.pub', '-I', uuid, '-z', serial, '-V', f'-1d:{expiration}', '-n', ','.join(args.principals), '-f', args.output, args.key_file], env=os.environ | {'SSH_AUTH_SOCK': ssh_auth_sock}, check=True)

            agent_proc.terminate()

    sleep(1)


if __name__ == '__main__':
    sys.exit(main())
